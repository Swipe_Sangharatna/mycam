
package com.swipe.mycamera;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;

import com.google.android.gms.vision.face.Face;
import com.swipe.mycamera.camera.GraphicOverlay;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Graphic instance for rendering face position, orientation, and landmarks within an associated
 * graphic overlay view.
 */
@SuppressWarnings("ALL")
class FaceGraphic extends GraphicOverlay.Graphic {
    private static final float FACE_POSITION_RADIUS = 10.0f;
    private static final float ID_TEXT_SIZE = 40.0f;
    private static final float ID_Y_OFFSET = 50.0f;
    private static final float ID_X_OFFSET = -50.0f;
    private static final float BOX_STROKE_WIDTH = 5.0f;

    /*private static final int COLOR_CHOICES[] = {
        Color.BLUE,
        Color.CYAN,
        Color.GREEN,
        Color.MAGENTA,
        Color.RED,
        Color.WHITE,
        Color.YELLOW
    };*/
    private static final int COLOR_CHOICES[] = {
            Color.WHITE,
            Color.WHITE,
            Color.WHITE,
            Color.WHITE,
            Color.WHITE,
            Color.WHITE,
            Color.WHITE
    };
    private static int mCurrentColorIndex = 0;

    private Paint mFacePositionPaint;
    private Paint mIdPaint;
    private Paint mBoxPaint;

    private volatile Face mFace;
    private int mFaceId;
    private float mFaceHappiness;


    float xx=0.0f;
    float yy=0.0f;

    FaceGraphic(GraphicOverlay overlay) {
        super(overlay);

        mCurrentColorIndex = (mCurrentColorIndex + 1) % COLOR_CHOICES.length;
        final int selectedColor = COLOR_CHOICES[mCurrentColorIndex];

        mFacePositionPaint = new Paint();
        mFacePositionPaint.setColor(selectedColor);

        mIdPaint = new Paint();
        mIdPaint.setColor(selectedColor);
        mIdPaint.setTextSize(ID_TEXT_SIZE);

        mBoxPaint = new Paint();
        mBoxPaint.setColor(selectedColor);
        mBoxPaint.setStyle(Paint.Style.STROKE);
        mBoxPaint.setStrokeWidth(BOX_STROKE_WIDTH);
    }

    void setId(int id) {
        mFaceId = id;
    }


    /**
     * Updates the face instance from the detection of the most recent frame.  Invalidates the
     * relevant portions of the overlay to trigger a redraw.
     */
    void updateFace(Face face) {
        mFace = face;
        postInvalidate();
    }

    /**
     * Draws the face annotations for position on the supplied canvas.
     */
    @Override
    public void draw(Canvas canvas) {
        Face face = mFace;



        if (face == null) {
            return;
        }



        // Draws a circle at the position of the detected face, with the face's track id below.
        float x = translateX(face.getPosition().x + face.getWidth() / 2);
        float y = translateY(face.getPosition().y + face.getHeight() / 2);
        xx=x;
        yy=y;
        canvas.drawCircle(x, y, FACE_POSITION_RADIUS, mFacePositionPaint);
        //canvas.drawText("id: " + mFaceId, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
        //canvas.drawText("happiness: " + String.format("%.2f", face.getIsSmilingProbability()), x - ID_X_OFFSET, y - ID_Y_OFFSET, mIdPaint);
        //canvas.drawText("right eye: " + String.format("%.2f", face.getIsRightEyeOpenProbability()), x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
        //canvas.drawText("left eye: " + String.format("%.2f", face.getIsLeftEyeOpenProbability()), x - ID_X_OFFSET*2, y - ID_Y_OFFSET*2, mIdPaint);


        //Log.e("FACE", "INIT");

        float happy = face.getIsSmilingProbability();
        if(happy>0.90) {
            if(myapp.Age==(null)) {
                canvas.drawText("Mood: " + "Happy", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);

            }
            else {
                canvas.drawText("Mood: " + "Happy", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
                canvas.drawText("Age : " + myapp.Age, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Gender: " + myapp.Gender,x + ID_X_OFFSET * 2, y - ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Race: " + myapp.Race, x + ID_X_OFFSET * 2, y - ID_Y_OFFSET*2, mIdPaint);
            }
            myapp.isHappy=true;

        }
        else if(happy<0.90 && happy > 0.35) {
            if(myapp.Age==(null))
            canvas.drawText("Mood: " + "Normal", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            else{
            canvas.drawText("Mood: " + "Normal", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            canvas.drawText("Age : " + myapp.Age, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Gender: " + myapp.Gender,x + ID_X_OFFSET * 2, y - ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Race: " + myapp.Race,x + ID_X_OFFSET * 2, y - ID_Y_OFFSET*2, mIdPaint);

        }
            myapp.isHappy = false;
        }
        else if(happy<0.35 && happy > 0.10){
            if(myapp.Age==(null))
            canvas.drawText("Mood: " + "Angry", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            else{
            canvas.drawText("Mood: " + "Angry", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
                canvas.drawText("Age : " + myapp.Age, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Gender: " + myapp.Gender, x + ID_X_OFFSET * 2, y - ID_Y_OFFSET, mIdPaint);
                canvas.drawText("Race: " + myapp.Race, x + ID_X_OFFSET * 2, y - ID_Y_OFFSET*2, mIdPaint);
    }
            myapp.isHappy = false;
        }
        else if(happy<0.10){
            if(myapp.Age==(null))
                canvas.drawText("Mood: " + "Sad", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            else{
            canvas.drawText("Mood: " + "Sad", x + ID_X_OFFSET * 2, y + ID_Y_OFFSET * 2, mIdPaint);
            canvas.drawText("Age : " + myapp.Age, x + ID_X_OFFSET, y + ID_Y_OFFSET, mIdPaint);
            canvas.drawText("Gender: " + myapp.Gender,x + ID_X_OFFSET * 2, y - ID_Y_OFFSET, mIdPaint);
            canvas.drawText("Race: " + myapp.Race,x + ID_X_OFFSET * 2, y - ID_Y_OFFSET*2, mIdPaint);
        }
            myapp.isHappy = false;
        }
        // Draws a bounding box around the face.
        float xOffset = scaleX(face.getWidth() / 2.0f);
        float yOffset = scaleY(face.getHeight() / 2.0f);
        float left = x - xOffset;
        float top = y - yOffset;
        float right = x + xOffset;
        float bottom = y + yOffset;
        canvas.drawRect(left, top, right, bottom, mBoxPaint);

       /* if(myapp.img!=null) {
            new CalculateRace().execute();
        }
        if(myapp.img2!=null){
            //myapp.face.setImageBitmap(myapp.img2);

            *//*BitmapFactory.Options options = new BitmapFactory.Options();
            //options.inJustDecodeBounds = false;
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            options.inDither = true;

            Bitmap cs = BitmapFactory.decodeFile(myapp.path,options);*//*


        }*/
    }


    public int getDominantColor1(Bitmap bitmap) {



        if (bitmap == null)
            throw new NullPointerException();

        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        int size = width * height;
        int pixels[] = new int[size];

        Bitmap bitmap2 = bitmap.copy(Bitmap.Config.ARGB_4444, false);

        bitmap2.getPixels(pixels, 0, width, 0, 0, width, height);

        final List<HashMap<Integer, Integer>> colorMap = new ArrayList<HashMap<Integer, Integer>>();
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());
        colorMap.add(new HashMap<Integer, Integer>());

        int color = 0;
        int r = 0;
        int g = 0;
        int b = 0;
        Integer rC, gC, bC;
        for (int i = 0; i < pixels.length; i++) {
            color = pixels[i];

            r = Color.red(color);
            g = Color.green(color);
            b = Color.blue(color);

            rC = colorMap.get(0).get(r);
            if (rC == null)
                rC = 0;
            colorMap.get(0).put(r, ++rC);

            gC = colorMap.get(1).get(g);
            if (gC == null)
                gC = 0;
            colorMap.get(1).put(g, ++gC);

            bC = colorMap.get(2).get(b);
            if (bC == null)
                bC = 0;
            colorMap.get(2).put(b, ++bC);
        }

        int[] rgb = new int[3];
        for (int i = 0; i < 3; i++) {
            int max = 0;
            int val = 0;
            for (Map.Entry<Integer, Integer> entry : colorMap.get(i).entrySet()) {
                if (entry.getValue() > max) {
                    max = entry.getValue();
                    val = entry.getKey();
                }
            }
            rgb[i] = val;
        }

        int dominantColor = Color.rgb(rgb[0], rgb[1], rgb[2]);

        Log.e("COLOR","R>>"+rgb[0]+" G"+rgb[1]+" B"+rgb[2]);

        return dominantColor;
    }

   /* private class CalculateRace extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {

            if(myapp.img!=null) {
               // Bitmap p = Bitmap.createBitmap(myapp.img,0,0,80,80);

                myapp.img2=myapp.img;
                //int value = getDominantColor1(myapp.img);
                //Log.e("Face Color", ">> " + value);
                myapp.race="HELLO";
            }
            return null;
        }
    }*/
}
