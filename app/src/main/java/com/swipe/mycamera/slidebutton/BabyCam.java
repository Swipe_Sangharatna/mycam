package com.swipe.mycamera.slidebutton;

import android.app.Activity;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.swipe.mycamera.CustomViews.DonutProgress;
import com.swipe.mycamera.R;
import com.swipe.mycamera.myapp;

import java.util.Timer;
import java.util.TimerTask;

public class BabyCam extends Activity {

    ImageView iv,iv1,iv2,iv3,iv4,iv5;
    int iv1flag=0;
    int iv2flag=0;
    int iv3flag=0;
    int iv4flag=0;
    int iv5flag=0;
    int iv6flag=0;
    int iv1codeflag=0;
    int iv2codeflag=0;
    int iv3codeflag=0;
    int iv4codeflag=0;
    int iv5codeflag=0;
    int iv6codeflag=0;
    int time=0;
    int max;
    Timer myTimer,myTimer1,myTimer2,myTimer3,myTimer4,myTimer5;

    GestureDetector gestureDetector;

    int tm = 0;
    boolean exeflag;
    DonutProgress progressBar,progressBar1,progressBar2,progressBar3,progressBar4,progressBar5;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.babycamview);

        myapp.mp = new MediaPlayer();

        iv = (ImageView) findViewById(R.id.imageView9);

        progressBar = (DonutProgress) findViewById(R.id.donut_progress);

        iv1 = (ImageView) findViewById(R.id.imageView91);
        iv2 = (ImageView) findViewById(R.id.imageView92);
        iv3 = (ImageView) findViewById(R.id.imageView913);
        iv4 = (ImageView) findViewById(R.id.imageView94);
        iv5 = (ImageView) findViewById(R.id.imageView915);

        progressBar1 = (DonutProgress) findViewById(R.id.donut_progress1);
        progressBar2 = (DonutProgress) findViewById(R.id.donut_progress2);
        progressBar3 = (DonutProgress) findViewById(R.id.donut_progress13);
        progressBar4 = (DonutProgress) findViewById(R.id.donut_progress4);
        progressBar5 = (DonutProgress) findViewById(R.id.donut_progress15);


        initValues();

        iv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv1flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv1flag == 1 && iv1codeflag == 0) {

                            iv1codeflag = 1;

                            myapp.addToSharedPreferances("PROGRESS", "P1");

                            reset();
                            time = 0;

                            try {

                                iv.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp = MediaPlayer.create(BabyCam.this, R.raw.baby1);

                                myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby1);
                                myapp.soundID = R.raw.baby1;

                                Log.e("MP", "MAX " + Math.round((float) myapp.mp.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp.getDuration() / 1000f);
                                myapp.mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar.setMax(Math.round((float) myapp.mp.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp.release();
                                    myapp.mp = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if (myTimer != null)
                                                myTimer.cancel();
                                            Log.e("TimerRelease", "MYTIMER");
                                            iv.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    }, 1000);
                                }
                            });
                            myapp.mp.start();

                            tm = 0;

                            myTimer = new Timer();
                            myTimer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod();
                                }
                            }, 0, 1000);
                        } else {

                            reset();
                            if (myapp.mp != null) {
                                myapp.mp.release();
                                myapp.mp = null;
                            }
                            if (myTimer != null) {
                                myTimer.cancel();
                            }
                            iv.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar.setMax(5);
                            progressBar.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P1");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby1);
                            Log.e("Double tap", "EXE");
                        }

                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv1codeflag = 0;
                        iv1flag = 0;
                    }
                }, 2200);
            }
        });

        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv2flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv2flag == 1 && iv2codeflag == 0) {

                            iv2codeflag=1;

                            reset();
                            time = 0;
                            myapp.addToSharedPreferances("PROGRESS","P2");
                            try {

                                iv1.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp1 = MediaPlayer.create(BabyCam.this, R.raw.baby2);
                                myapp.addToSharedPreferances("SOUNDID",""+R.raw.baby2);
                                myapp.soundID = R.raw.baby2;

                                Log.e("MP", "MAX " + Math.round((float) myapp.mp1.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp1.getDuration() / 1000f);
                                myapp.mp1.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar1.setMax(Math.round((float) myapp.mp1.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp1.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp1.release();
                                    myapp.mp1 = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {

                                            if(myTimer1!=null)
                                                myTimer1.cancel();
                                            Log.e("TimerRelease","MYTIMER");
                                            iv1.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    },1000);
                                }
                            });
                            myapp.mp1.start();

                            tm = 0;

                            myTimer1 = new Timer();
                            myTimer1.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod1();
                                }
                            }, 0, 1000);
                        }
                        else
                        {
                            reset();
                            if(myapp.mp1!=null)
                            {
                                myapp.mp1.release();
                                myapp.mp1=null;
                            }
                            if(myTimer1!=null)
                            {
                                myTimer1.cancel();
                            }
                            iv1.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar1.setMax(5);
                            progressBar1.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P2");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby2);
                            Log.e("Double tap","EXE");
                        }

                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv2codeflag=0;
                        iv2flag=0;
                    }
                },2200);
            }
        });

        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv3flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv3flag == 1 && iv3codeflag == 0) {

                            iv3codeflag=1;

                            reset();
                            time = 0;
                            myapp.addToSharedPreferances("PROGRESS","P3");
                            try {

                                iv2.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp2 = MediaPlayer.create(BabyCam.this, R.raw.baby3);
                                myapp.addToSharedPreferances("SOUNDID",""+R.raw.baby3);
                                myapp.soundID = R.raw.baby3;

                                Log.e("MP", "MAX " + Math.round((float) myapp.mp2.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp2.getDuration() / 1000f);
                                myapp.mp2.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar2.setMax(Math.round((float) myapp.mp2.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp2.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp2.release();
                                    myapp.mp2 = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(myTimer2!=null)
                                                myTimer2.cancel();
                                            Log.e("TimerRelease","MYTIMER");
                                            iv2.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    },1000);
                                }
                            });
                            myapp.mp2.start();

                            tm = 0;

                            myTimer2 = new Timer();
                            myTimer2.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod2();
                                }
                            }, 0, 1000);

                        }
                        else
                        {
                            reset();
                            if(myapp.mp2!=null)
                            {
                                myapp.mp2.release();
                                myapp.mp2=null;
                            }
                            if(myTimer2!=null)
                            {
                                myTimer2.cancel();
                            }
                            iv2.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar2.setMax(5);
                            progressBar2.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P3");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby3);
                            Log.e("Double tap","EXE");
                        }

                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv3codeflag=0;
                        iv3flag=0;
                    }
                },2200);
            }
        });

        iv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv4flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv4flag == 1 && iv4codeflag == 0) {

                            iv4codeflag=1;

                            reset();
                            time = 0;
                            myapp.addToSharedPreferances("PROGRESS","P4");
                            try {

                                iv3.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp3 = MediaPlayer.create(BabyCam.this, R.raw.baby4);

                                myapp.addToSharedPreferances("SOUNDID",""+R.raw.baby4);
                                myapp.soundID = R.raw.baby4;

                                Log.e("MP", "MAX " + Math.round((float) myapp.mp3.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp3.getDuration() / 1000f);
                                myapp.mp3.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar3.setMax(Math.round((float) myapp.mp3.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp3.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp3.release();
                                    myapp.mp3 = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(myTimer3!=null)
                                                myTimer3.cancel();
                                            Log.e("TimerRelease","MYTIMER");
                                            iv3.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    },1000);
                                }
                            });
                            myapp.mp3.start();

                            tm = 0;

                            myTimer3 = new Timer();
                            myTimer3.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod3();
                                }
                            }, 0, 1000);

                        }
                        else
                        {
                            reset();
                            if(myapp.mp3!=null)
                            {
                                myapp.mp3.release();
                                myapp.mp3=null;
                            }
                            if(myTimer3!=null)
                            {
                                myTimer3.cancel();
                            }
                            iv3.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar3.setMax(5);
                            progressBar3.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P4");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby4);
                            Log.e("Double tap","EXE");
                        }

                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv4codeflag=0;
                        iv4flag=0;
                    }
                },2200);
            }
        });
        iv4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv5flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv5flag == 1 && iv5codeflag == 0) {

                            iv5codeflag=1;
                            reset();
                            time = 0;
                            myapp.addToSharedPreferances("PROGRESS","P5");
                            try {

                                iv4.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp4 = MediaPlayer.create(BabyCam.this, R.raw.baby5);

                                myapp.addToSharedPreferances("SOUNDID",""+R.raw.baby5);

                                myapp.soundID = R.raw.baby5;
                                Log.e("MP", "MAX " + Math.round((float) myapp.mp4.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp4.getDuration() / 1000f);
                                myapp.mp4.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar4.setMax(Math.round((float) myapp.mp4.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp4.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp4.release();
                                    myapp.mp4 = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(myTimer4!=null)
                                                myTimer4.cancel();
                                            Log.e("TimerRelease","MYTIMER");
                                            iv4.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    },1000);
                                }
                            });
                            myapp.mp4.start();

                            tm = 0;

                            myTimer4 = new Timer();
                            myTimer4.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod4();
                                }
                            }, 0, 1000);

                        }
                        else
                        {
                            reset();
                            if(myapp.mp4!=null)
                            {
                                myapp.mp4.release();
                                myapp.mp4=null;
                            }
                            if(myTimer4!=null)
                            {
                                myTimer4.cancel();
                            }
                            iv4.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar4.setMax(5);
                            progressBar4.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P5");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby5);
                            Log.e("Double tap","EXE");
                        }
                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv5codeflag=0;
                        iv5flag=0;
                    }
                },2200);
            }
        });

        iv5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                iv6flag++;
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        if (iv6flag == 1 && iv6codeflag == 0) {

                            iv6codeflag=1;
                            reset();
                            time = 0;
                            myapp.addToSharedPreferances("PROGRESS","P6");
                            try {

                                iv5.setImageDrawable(getResources().getDrawable(R.mipmap.ply1));
                                myapp.mp5 = MediaPlayer.create(BabyCam.this, R.raw.baby6);

                                myapp.addToSharedPreferances("SOUNDID",""+R.raw.baby6);
                                myapp.soundID = R.raw.baby6;

                                Log.e("MP", "MAX " + Math.round((float) myapp.mp5.getDuration() / 1000f));
                                max = Math.round((float) myapp.mp5.getDuration() / 1000f);
                                myapp.mp5.setAudioStreamType(AudioManager.STREAM_MUSIC);
                                progressBar5.setMax(Math.round((float) myapp.mp5.getDuration() / 1000f));

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            myapp.mp5.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp) {

                                    myapp.mp5.release();
                                    myapp.mp5 = null;
                                    Log.e("MP", "RELEASE");
                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            if(myTimer5!=null)
                                                myTimer5.cancel();
                                            Log.e("TimerRelease","MYTIMER");
                                            iv5.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                                        }
                                    },1000);
                                }
                            });
                            myapp.mp5.start();

                            tm = 0;
                            myTimer5 = new Timer();
                            myTimer5.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    TimerMethod5();
                                }
                            }, 0, 1000);

                        }
                        else
                        {
                            reset();
                            if(myapp.mp5!=null)
                            {
                                myapp.mp5.release();
                                myapp.mp5=null;
                            }
                            if(myTimer5!=null)
                            {
                                myTimer5.cancel();
                            }
                            iv5.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
                            progressBar5.setMax(5);
                            progressBar5.setProgress(5);
                            myapp.addToSharedPreferances("PROGRESS", "P6");
                            myapp.addToSharedPreferances("SOUNDID", "" + R.raw.baby6);
                            Log.e("Double tap","EXE");
                        }
                    }
                }, 1000);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        iv6codeflag=0;
                        iv6flag=0;
                    }
                },2200);
            }
        });
    }

    private void initValues() {

        if(myapp.getStringFromSharedPreferances("PROGRESS")!=null)
        {
            String s = myapp.getStringFromSharedPreferances("PROGRESS");
            if(s.equals("P1"))
            {
                progressBar.setMax(100);
                progressBar.setProgress(100);
            }else if(s.equals("P2"))
            {
                progressBar1.setMax(100);
                progressBar1.setProgress(100);
            }else if(s.equals("P3"))
            {
                progressBar2.setMax(100);
                progressBar2.setProgress(100);
            }else if(s.equals("P4"))
            {
                progressBar3.setMax(100);
                progressBar3.setProgress(100);
            }else if(s.equals("P5"))
            {
                progressBar4.setMax(100);
                progressBar4.setProgress(100);
            }else if(s.equals("P6"))
            {
                progressBar5.setMax(100);
                progressBar5.setProgress(100);
            }
        }
    }
    public void reset()
    {
        try{
            if(myapp.mp!=null)
            {
                myapp.mp.release();
                myapp.mp = null;
            }
            if(myTimer!=null)
            myTimer.cancel();
            progressBar.setProgress(0);
        }catch (Exception e)
        {}
        try{
            if(myapp.mp1!=null)
            {
                myapp.mp1.release();
                myapp.mp1 = null;
            }
            if(myTimer1!=null)
                myTimer1.cancel();
            progressBar1.setProgress(0);
        }catch (Exception e)
        {}
        try{
            if(myapp.mp2!=null)
            {
                myapp.mp2.release();
                myapp.mp2 = null;
            }
            if(myTimer2!=null)
                myTimer2.cancel();
            progressBar2.setProgress(0);
        }catch (Exception e)
        {}
        try{
            if(myapp.mp3!=null)
            {
                myapp.mp3.release();
                myapp.mp3 = null;
            }
            if(myTimer3!=null)
                myTimer3.cancel();
            progressBar3.setProgress(0);
        }catch (Exception e)
        {}
        try{
            if(myapp.mp4!=null)
            {
                myapp.mp4.release();
                myapp.mp4 = null;
            }
            if(myTimer4!=null)
                myTimer4.cancel();
            progressBar4.setProgress(0);
        }catch (Exception e)
        {}
        try{
            if(myapp.mp5!=null)
            {
                myapp.mp5.release();
                myapp.mp5 = null;
            }
            if(myTimer5!=null)
                myTimer5.cancel();
            progressBar5.setProgress(0);
        }catch (Exception e)
        {}
        iv1.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
        iv2.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
        iv3.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
        iv4.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
        iv5.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
        iv.setImageDrawable(getResources().getDrawable(R.mipmap.ply2));
    }
    private void TimerMethod()
    {
        this.runOnUiThread(Timer_Tick);
    }
    private Runnable Timer_Tick = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };
    private void TimerMethod1()
    {
        this.runOnUiThread(Timer_Tick1);
    }
    private Runnable Timer_Tick1 = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar1.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };
    private void TimerMethod2()
    {
        this.runOnUiThread(Timer_Tick2);
    }
    private Runnable Timer_Tick2 = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar2.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };
    private void TimerMethod3()
    {
        this.runOnUiThread(Timer_Tick3);
    }
    private Runnable Timer_Tick3 = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar3.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };
    private void TimerMethod4()
    {
        this.runOnUiThread(Timer_Tick4);
    }
    private Runnable Timer_Tick4 = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar4.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };

    private void TimerMethod5()
    {
        this.runOnUiThread(Timer_Tick5);
    }
    private Runnable Timer_Tick5 = new Runnable() {

        public void run() {
            if(tm<=max)
            {
                progressBar5.setProgress(tm);
                tm++;
                Log.e("PRO",">> "+tm);
            }
        }
    };
}
