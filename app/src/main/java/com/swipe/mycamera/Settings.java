package com.swipe.mycamera;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.swipe.mycamera.slidebutton.SwitchButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("ALL")
public class Settings extends Activity {

    //capture settings

    private RadioGroup mWhiteBalRadioGroup,mPictureSizeRadioGroup,mPictureSizeRadioGroup_elite2,mSettingRadioGroup;
    private RelativeLayout WhiteBalSetting_Layout,mSettingRelativeLayout;
    private LinearLayout mPictureLinearLayout,mPictureLinearLayout_elite2,mLineraLayout;
    private View settingPictureSize,settingPictureSize_elite2,mSettingLayout;
    private ArrayList<RadioButton> radiobutton;
    private ArrayList<View> mViewsList;
    private  ArrayList<Integer> mPictureSizeValues;
    private ArrayList<Integer> mElementList;
    private RadioButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14;
    private View v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,v11,v12,v13,v14;
    private int flagPictureSize=0,flagSelfTimer=0,flagPictureQuality=0, flagIso=0,flagExposure=0,flagColor=0,flagScene=0,flagWhiteBal=0,flagAntiflicker=0;
    private ImageView mTimerValue;
    private TextView mPictureSizeValue,mPictureQualityValue,mIsoValue;

    RelativeLayout ExposurLayout,mPictureSizeLayout,mPictureQuality_Layout,mGeneral_Layout_Container;
    ScrollView whiteBalLayout;

    View mAuto,mIncandscent,mDaylihgt,mFlurescent,mCloudy,mTwilight,mShade,mWarmFluro,settingWhiteBal,mGeneralView;
    private TextView mFaceDetection,mSmileShot,mAutoScene,mSelfTimerView,mPictureSizeView,mIso,mFaceBeauty,
            mExpoPlustwo,mExpoPLusThree;
    private TextView mGpsTxtView,mExposureTxtView,mColorEffectTxtView,mSceneModeTxtView,mWhiteBalTxtView,mAntiFlickerTxtView, DefaultRestoreTxtView;

    private TextView mPictureQuality,mHDROnOff,mAntiFlicker,mSelfTimer;
    private Camera.Parameters whiteBalParametrs;
    private int QVGA,VGA;
    private ListView mPictureListView,mPicQualityList;
    private ArrayList<Integer> mPixelList;
    private LinkedHashMap<Integer,Camera.Size> mList;
    private ArrayList<Camera.Size> mPixelParamList;
    private Camera.Parameters mCameraParametrs;
    private int flagCounter,counter_exposure=0,counter_picturequality=0,counter_RelativeLayout=0;
    int mToggleSwitch_picQuality=0,mToggleSwitch_Exposure=0,mToggleSwitch_pictureSize=0,mToggleSwitch_Iso=0,mToggleSwitch_Hdr=0,mToggleSwitch_AntiFlicker=0;
    int counter_iso=0;
    LayoutInflater inflater;
    private int BackPressedVal=0;
    private ArrayList<String> pixelArrayList;

    private int flagSelfTimerSRadio=0;

    //
    ImageView capture,video,general;
    Color selSetting;
    RelativeLayout captureLayout,videoLayout,generalLayout,rootlay;

    SwitchButton hdrSwitch,noiseSwitch,microphoneSwitch,slideButtonShutter;

    TextView storageLocation;
    SwitchButton touchToFocusSwitch,slideButtonFace,slideButtonSmile,slideButtonQR,slidebaby;

    AlertDialog storageLocAD,resolutionAD,qualityAD;

    View blue1,blue2,blue3;

    TextView resolution,quality;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initViews();
        initListeners();
        setValues();


    }

    private void setValues() {
        if(myapp.getStringFromSharedPreferances("SHUTTER")!=null)
        {
            if(myapp.getStringFromSharedPreferances("SHUTTER").equals("ON"))
            {
                slideButtonShutter.setChecked(true);
            }
            else
            {
                slideButtonShutter.setChecked(false);
            }
        }

        if(myapp.getStringFromSharedPreferances("BABY")!=null)
        {
            if(myapp.getStringFromSharedPreferances("BABY").equals("ON"))
            {
                slidebaby.setChecked(true);
            }
            else
            {
                slidebaby.setChecked(false);
            }
        }


        if(myapp.getStringFromSharedPreferances("QR")!=null)
        {
            if(myapp.getStringFromSharedPreferances("QR").equals("ON"))
            {
                slideButtonQR.setChecked(true);
            }
            else
            {
                slideButtonQR.setChecked(false);
            }
        }


        if(myapp.getStringFromSharedPreferances("HDR")!=null)
        {
            if(myapp.getStringFromSharedPreferances("HDR").equalsIgnoreCase("ON"))
            {
               hdrSwitch.setChecked(true);
            }
        }
        if(myapp.getStringFromSharedPreferances("FOCUS")!=null)
        {
            if(myapp.getStringFromSharedPreferances("FOCUS").equalsIgnoreCase("ON"))
            {
                touchToFocusSwitch.setChecked(true);
            }
        }
        if(myapp.getStringFromSharedPreferances("RESOLUTION")!=null)
        {
            resolution.setText(myapp.getStringFromSharedPreferances("RESOLUTION"));
        }
        if(myapp.getStringFromSharedPreferances("QUALITY")!=null)
        {
           quality.setText(myapp.getStringFromSharedPreferances("QUALITY"));
        }

        if(myapp.getStringFromSharedPreferances("NOISE")!=null)
        if(myapp.getStringFromSharedPreferances("NOISE").equalsIgnoreCase("NOTALLOW"))
            noiseSwitch.setChecked(true);
        else
            noiseSwitch.setChecked(false);



        if(myapp.getStringFromSharedPreferances("MICROPHONE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("MICROPHONE").equalsIgnoreCase("ON"))
            {
                microphoneSwitch.setChecked(true);
                myapp.isMicrophoneAllow=true;
            }
            else {
                microphoneSwitch.setChecked(false);
                myapp.isMicrophoneAllow=false;
            }
        }

        if(myapp.getStringFromSharedPreferances("FACE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("FACE").equalsIgnoreCase("ON"))
            {
                slideButtonFace.setChecked(true);
                slideButtonSmile.setEnabled(true);

            }
            else {
                slideButtonFace.setChecked(false);
                slideButtonSmile.setEnabled(false);
            }
        }
        else
        {
            slideButtonFace.setChecked(false);
            slideButtonSmile.setEnabled(false);
        }
        if(myapp.getStringFromSharedPreferances("SMILE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("SMILE").equalsIgnoreCase("ON"))
            {
                slideButtonSmile.setChecked(true);

            }
            else {
               slideButtonSmile.setChecked(false);
            }
        }

        initCapture();
        captureLayout.setVisibility(View.VISIBLE);

    }
    private void initCapture() {

        mCameraParametrs=myapp.mCamera.getParameters();

        inflater= (LayoutInflater)Settings.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        init();

        mWhiteBalTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer = 0;
                    flagPictureSize = 0;
                    flagIso = 0;
                    flagPictureQuality = 0;
                    flagAntiflicker = 0;
                    flagScene = 0;
                    flagColor = 0;
                    flagExposure = 0;
                    if (radiobutton != null) {
                        makeViewsNull(mGeneral_Layout_Container);
                    }
                }
                if (flagWhiteBal == 0) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeWhiteBalView();
                    mWhiteBalTxtView.setTextColor(Color.parseColor("#3f51b5"));

                } else if (flagWhiteBal == 1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagWhiteBal = 0;
                    mWhiteBalTxtView.setTextColor(Color.WHITE);
                } else if (flagWhiteBal == 2) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeWhiteBalView();
                    mWhiteBalTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }
        });
        mExposureTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer = 0;
                    flagPictureSize = 0;
                    flagIso = 0;
                    flagPictureQuality = 0;
                    flagAntiflicker = 0;
                    flagScene = 0;
                    flagColor = 0;
                    flagWhiteBal = 0;
                    if (radiobutton != null) {
                        makeViewsNull(mGeneral_Layout_Container);
                    }
                }
                if (flagExposure == 0) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeExposureView();
                    mExposureTxtView.setTextColor(Color.parseColor("#3f51b5"));

                } else if (flagExposure==1){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagExposure = 0;
                    mExposureTxtView.setTextColor(Color.WHITE);
                }
                else if (flagExposure==2){
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeExposureView();
                    mExposureTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }
        });
        mPictureQualityValue= (TextView)findViewById(R.id.pictureQualityValue);

        if(myapp.getStringFromSharedPreferencesValues("PICTUREQUALITY_STR")!=null){
            String variable=myapp.getStringFromSharedPreferances("PICTUREQUALITY_STR");
            mPictureQualityValue.setText(variable);
            mPictureQualityValue.setTextColor(Color.WHITE);
        }
        mPictureQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout!=null){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer=0;
                    flagPictureSize=0;
                    flagIso=0;

                    flagAntiflicker=0;
                    flagScene=0;
                    flagColor=0;
                    flagWhiteBal=0;
                    flagExposure=0;
                    if (radiobutton!=null){
                        makeViewsNull(captureLayout);
                    }
                }
                if (flagPictureQuality==0){
                    makeViewsNull(captureLayout);
                    initializePicQualityView();
                    mPictureQuality.setTextColor(Color.parseColor("#3f51b5"));

                }else if(flagPictureQuality==1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagPictureQuality=0;
                    mPictureQuality.setTextColor(Color.WHITE);
                }
                else if (flagPictureQuality==2){
                    makeViewsNull(captureLayout);
                    initializePicQualityView();
                    mPictureQuality.setTextColor(Color.parseColor("#3f51b5"));

                }
            }
        });
        mIsoValue= (TextView)findViewById(R.id.isoValue);

        if(myapp.getStringFromSharedPreferencesValues("ISO_STR")!=null){
            String variable=myapp.getStringFromSharedPreferances("ISO_STR");
            mIsoValue.setText(variable);
            mIsoValue.setTextColor(Color.WHITE);
        }

        mIso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer = 0;
                    flagPictureSize = 0;

                    flagPictureQuality = 0;
                    flagAntiflicker = 0;
                    flagScene = 0;
                    flagColor = 0;
                    flagWhiteBal = 0;
                    flagExposure = 0;
                    if (radiobutton != null) {
                        makeViewsNull(captureLayout);
                    }
                }
                if (flagIso == 0) {
                    makeViewsNull(captureLayout);
                    initilizeIsoView();
                    mIso.setTextColor(Color.parseColor("#3f51b5"));

                } else if (flagIso == 1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagIso = 0;
                    mIso.setTextColor(Color.WHITE);
                } else if (flagIso == 2) {
                    makeViewsNull(captureLayout);
                    initilizeIsoView();
                    mIso.setTextColor(Color.parseColor("#3f51b5"));
                }
            }
        });

        mPictureSizeValue= (TextView)findViewById(R.id.picturesizeValue);

        if(myapp.getStringFromSharedPreferencesValues("PICTURESIZE_STR")!=null){
            String variable=myapp.getStringFromSharedPreferances("PICTURESIZE_STR");
            mPictureSizeValue.setText(variable);
            mPictureQualityValue.setTextColor(Color.WHITE);
        }


        mPictureSizeView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout!=null){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer=0;

                    flagIso=0;
                    flagPictureQuality=0;
                    flagAntiflicker=0;
                    flagScene=0;
                    flagColor=0;
                    flagWhiteBal=0;
                    flagExposure=0;
                    if (radiobutton!=null){
                        makeViewsNull(captureLayout);
                    }
                }
                if (flagPictureSize==0){
                    makeViewsNull(captureLayout);
                    pictureSizeMethod();
                    mPictureSizeView.setTextColor(Color.parseColor("#3f51b5"));

                }else if (flagPictureSize==1){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagPictureSize=0;
                    mPictureSizeView.setTextColor(Color.WHITE);
                }
                else if (flagPictureSize==2){
                    makeViewsNull(captureLayout);
                    pictureSizeMethod();
                    mPictureSizeView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }
        });
        mAntiFlickerTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout!=null){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer=0;
                    flagPictureSize=0;
                    flagIso=0;
                    flagPictureQuality=0;

                    flagScene=0;
                    flagColor=0;
                    flagWhiteBal=0;
                    flagExposure=0;
                    if (radiobutton!=null){
                        makeViewsNull(mGeneral_Layout_Container);
                    }
                }
                if (flagAntiflicker==0){
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeAntiFlickerView();
                    mAntiFlickerTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }else if (flagAntiflicker==1){
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagAntiflicker=0;
                    mAntiFlickerTxtView.setTextColor(Color.WHITE);
                }
                else if (flagAntiflicker==2){
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeAntiFlickerView();
                    mAntiFlickerTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }

        });
        mSelfTimerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagSelfTimer=0;
                    flagPictureSize = 0;
                    flagIso = 0;
                    flagPictureQuality = 0;
                    flagAntiflicker = 0;
                    flagScene = 0;
                    flagColor = 0;
                    flagWhiteBal = 0;
                    flagExposure = 0;
                    if (radiobutton != null) {
                        makeViewsNull(captureLayout);
                    }
                }
                if (flagSelfTimer == 0) {
                    makeViewsNull(captureLayout);
                    initializeSelfTimer();
                    mSelfTimerView.setTextColor(Color.parseColor("#3f51b5"));
                    Log.e("flagSelfTimer:","flagSelfTimer==0");

                } else if (flagSelfTimer == 1 && flagSelfTimerSRadio!=1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagSelfTimer = 0;
                    mSelfTimerView.setTextColor(Color.WHITE);
                    Log.e("flagSelfTimer:", "flagSelfTimer==1 && flagradio!=1");
                } else if (flagSelfTimer == 2){
                    makeViewsNull(captureLayout);
                    initializeSelfTimer();
                    mSelfTimerView.setTextColor(Color.parseColor("#3f51b5"));
                    Log.e("flagSelfTimer:", "flagSelfTimer==0");
                }else if (flagSelfTimerSRadio==1){
                    makeViewsNull(captureLayout);
                    initializeSelfTimer();
                    mSelfTimerView.setTextColor(Color.parseColor("#3f51b5"));
                    flagSelfTimerSRadio=0;
                    flagSelfTimer=0;
                    Log.e("flagSelfTimer:", "flagSelfTimerSRadio==1");
                }
            }
        });
        mColorEffectTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer = 0;
                    flagPictureSize = 0;
                    flagIso = 0;
                    flagPictureQuality = 0;
                    flagAntiflicker = 0;
                    flagScene = 0;

                    flagWhiteBal = 0;
                    flagExposure = 0;
                    if (radiobutton != null) {
                        makeViewsNull(mGeneral_Layout_Container);
                    }
                }
                if (flagColor == 0) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeColorEffect();
                    mColorEffectTxtView.setTextColor(Color.parseColor("#3f51b5"));
                } else if (flagColor == 1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagColor = 0;
                    mColorEffectTxtView.setTextColor(Color.WHITE);
                } else if (flagColor == 2) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeColorEffect();
                    mColorEffectTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }

        });
        mSceneModeTxtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSettingRelativeLayout != null) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    // flagPictureSize=0;
                    flagSelfTimer = 0;
                    flagPictureSize = 0;
                    flagIso = 0;
                    flagPictureQuality = 0;
                    flagAntiflicker = 0;

                    flagColor = 0;
                    flagWhiteBal = 0;
                    flagExposure = 0;
                    if (radiobutton != null) {
                        makeViewsNull(mGeneral_Layout_Container);
                    }
                }
                if (flagScene == 0) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeSceneModes();
                    mSceneModeTxtView.setTextColor(Color.parseColor("#3f51b5"));
                } else if (flagScene == 1) {
                    mSettingRelativeLayout.setVisibility(View.GONE);
                    flagScene = 0;
                    mSceneModeTxtView.setTextColor(Color.WHITE);
                } else if (flagScene == 2) {
                    makeViewsNull(mGeneral_Layout_Container);
                    initializeSceneModes();
                    mSceneModeTxtView.setTextColor(Color.parseColor("#3f51b5"));
                }
            }
        });
        Camera.Parameters cam=myapp.mCamera.getParameters();
    }

    private void initListeners() {

        slidebaby.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    myapp.addToSharedPreferances("BABY","ON");
                    myapp.QR=true;
                }
                else {
                    myapp.addToSharedPreferances("BABY","OFF");
                    myapp.QR=false;
                }
            }
        });


        slideButtonQR.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    myapp.addToSharedPreferances("QR","ON");
                    myapp.QR=true;
                }
                else {
                    myapp.addToSharedPreferances("QR","OFF");
                    myapp.QR=false;
                }
            }
        });
        noiseSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    myapp.addToSharedPreferances("NOISE","NOTALLOW");
                }
                else
                    myapp.addToSharedPreferances("NOISE","ALLOW");

            }
        });

        quality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder ab = new AlertDialog.Builder(Settings.this, R.style.AppCompatAlertDialogStyle);
                ab.setTitle("Quality");
                View storageView = getLayoutInflater().inflate(R.layout.quality, null);

                RadioGroup rg = (RadioGroup) storageView.findViewById(R.id.rg2);

                RadioButton low = (RadioButton) storageView.findViewById(R.id.radioButton16);
                RadioButton medium = (RadioButton) storageView.findViewById(R.id.radioButton17);
                RadioButton high = (RadioButton) storageView.findViewById(R.id.radioButton18);

                if (myapp.getStringFromSharedPreferances("QUALITY") != null) {

                    if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("Low")) {
                        low.setChecked(true);
                    } else if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("Medium")) {
                        medium.setChecked(true);
                    } else if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("High")) {
                        high.setChecked(true);
                    }
                } else {
                    high.setChecked(true);
                }
                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.radioButton16) {
                            myapp.addToSharedPreferances("QUALITY", "Low");
                            myapp.BITRATE = 500000;
                            Log.e("HELLO", "LOWQUL");
                            quality.setText("Low");

                        } else if (checkedId == R.id.radioButton17) {
                            myapp.addToSharedPreferances("QUALITY", "Medium");
                            myapp.BITRATE = 2500000;
                            quality.setText("Medium");
                        } else if (checkedId == R.id.radioButton18) {
                            myapp.addToSharedPreferances("QUALITY", "High");
                            myapp.BITRATE = 8000000;
                            quality.setText("High");
                        }
                    }
                });
                ab.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        qualityAD.dismiss();
                    }
                });
                ab.setView(storageView);

                qualityAD = ab.create();
                qualityAD.show();
            }
        });

        resolution.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder ab = new AlertDialog.Builder(Settings.this,R.style.AppCompatAlertDialogStyle);
                ab.setTitle("Resolution");
                View storageView = getLayoutInflater().inflate(R.layout.resolution,null);

                RadioGroup rg = (RadioGroup) storageView.findViewById(R.id.rgs);

                RadioButton p360 = (RadioButton) storageView.findViewById(R.id.radioButton10);
                RadioButton p480 = (RadioButton) storageView.findViewById(R.id.radioButton11);
                RadioButton p720 = (RadioButton) storageView.findViewById(R.id.radioButton12);
                RadioButton p1080 = (RadioButton) storageView.findViewById(R.id.radioButton13);

                if(myapp.getStringFromSharedPreferances("RESOLUTION")!=null) {

                    if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("360P")) {
                        p360.setChecked(true);
                    } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("480P")) {
                        p480.setChecked(true);
                    } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("720P")) {
                        p720.setChecked(true);
                    } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("1080P")) {
                        p1080.setChecked(true);
                    }
                }
                else {
                    p1080.setChecked(true);
                }

                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if(checkedId==R.id.radioButton10)
                        {
                            myapp.addToSharedPreferances("RESOLUTION","360p");
                            myapp.WIDTH = myapp.WIDTH_240P;
                            myapp.HEIGHT = myapp.HEIGHT_240P;
                            resolution.setText("360p");

                        }else
                        if(checkedId==R.id.radioButton11)
                        {
                            myapp.addToSharedPreferances("RESOLUTION","480p");
                            myapp.WIDTH = myapp.WIDTH_480P;
                            myapp.HEIGHT = myapp.HEIGHT_480P;
                            resolution.setText("480p");
                        }else
                        if(checkedId==R.id.radioButton12)
                        {
                            myapp.addToSharedPreferances("RESOLUTION","720p");
                            myapp.WIDTH = myapp.WIDTH_720P;
                            myapp.HEIGHT = myapp.HEIGHT_720P;
                            resolution.setText("720p");
                        }else
                        if(checkedId==R.id.radioButton13)
                        {
                            myapp.addToSharedPreferances("RESOLUTION","1080p");
                            myapp.WIDTH = myapp.WIDTH_1080P;
                            myapp.HEIGHT = myapp.HEIGHT_1080P;
                            resolution.setText("1080p");
                        }
                    }
                });
                ab.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        resolutionAD.dismiss();
                    }
                });
                ab.setView(storageView);

                resolutionAD = ab.create();
                resolutionAD.show();
            }
        });

        touchToFocusSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    myapp.addToSharedPreferances("FOCUS", "ON");
                } else {
                    myapp.addToSharedPreferances("FOCUS", "OFF");
                }

            }
        });
       hdrSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                {
                    myapp.addToSharedPreferances("HDR","ON");
                    Camera.Parameters params = myapp.mCamera.getParameters();
                    params.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
                    myapp.mCamera.setParameters(params);
                }
                else {
                    myapp.addToSharedPreferances("HDR","OFF");
                    Camera.Parameters params = myapp.mCamera.getParameters();
                    params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                    myapp.mCamera.setParameters(params);
                }
            }
        });


        storageLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder ab = new AlertDialog.Builder(Settings.this, R.style.AppCompatAlertDialogStyle);
                ab.setTitle("Storage Locations");
                View storageView = getLayoutInflater().inflate(R.layout.storage, null);

                RadioGroup rg = (RadioGroup) storageView.findViewById(R.id.rg);

                RadioButton internal = (RadioButton) storageView.findViewById(R.id.radioButton);
                RadioButton external = (RadioButton) storageView.findViewById(R.id.radioButton2);

                if (myapp.isMemoryCardPresent) {
                    external.setEnabled(true);
                    external.setClickable(true);

                } else {
                    external.setEnabled(false);
                    external.setClickable(false);
                    external.setTextColor(Color.GRAY);
                }
                if (myapp.getStringFromSharedPreferances("STORAGE") != null) {
                    if (myapp.getStringFromSharedPreferances("STORAGE").equalsIgnoreCase("PHONE"))
                        internal.setChecked(true);
                    else
                        external.setChecked(true);
                } else
                    internal.setChecked(true);

                rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (checkedId == R.id.radioButton) {
                            myapp.addToSharedPreferances("STORAGE", "PHONE");
                        } else if (checkedId == R.id.radioButton2) {
                            myapp.addToSharedPreferances("STORAGE", "SDCARD");
                        }
                    }
                });

                ab.setView(storageView);
                ab.setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        storageLocAD.dismiss();
                    }
                });
                storageLocAD = ab.create();
                storageLocAD.show();
            }
        });

        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalLayout.setVisibility(View.GONE);
                videoLayout.setVisibility(View.GONE);
                captureLayout.setVisibility(View.VISIBLE);

                blue1.setVisibility(View.VISIBLE);
                blue2.setVisibility(View.INVISIBLE);
                blue3.setVisibility(View.INVISIBLE);
            }
        });
        video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                generalLayout.setVisibility(View.GONE);
                videoLayout.setVisibility(View.VISIBLE);
                captureLayout.setVisibility(View.GONE);

                blue1.setVisibility(View.INVISIBLE);
                blue2.setVisibility(View.VISIBLE);
                blue3.setVisibility(View.INVISIBLE);

            }
        });
        general.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                generalLayout.setVisibility(View.VISIBLE);
                videoLayout.setVisibility(View.GONE);
                captureLayout.setVisibility(View.GONE);

                blue1.setVisibility(View.INVISIBLE);
                blue2.setVisibility(View.INVISIBLE);
                blue3.setVisibility(View.VISIBLE);
            }
        });
        rootlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        microphoneSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    myapp.addToSharedPreferances("MICROPHONE", "ON");
                    myapp.isMicrophoneAllow = true;
                } else {
                    myapp.addToSharedPreferances("MICROPHONE", "OFF");
                    myapp.isMicrophoneAllow = false;
                }
            }
        });

        slideButtonFace.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    myapp.addToSharedPreferances("FACE", "ON");
                    slideButtonSmile.setEnabled(true);
                } else {
                    myapp.addToSharedPreferances("FACE", "OFF");
                    slideButtonSmile.setChecked(false);
                    slideButtonSmile.setEnabled(false);
                }
            }
        });

        slideButtonSmile.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    myapp.addToSharedPreferances("SMILE", "ON");
                } else {
                    myapp.addToSharedPreferances("SMILE", "OFF");
                }
            }
        });
        slideButtonShutter.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if(isChecked)
                {
                    myapp.addToSharedPreferances("SHUTTER", "ON");
                }
                else {
                    myapp.addToSharedPreferances("SHUTTER", "OFF");
                }
            }
        });

    }

    private void initViews() {

        capture = (ImageView) findViewById(R.id.imageView);
        video = (ImageView) findViewById(R.id.imageView2);
        general = (ImageView) findViewById(R.id.imageView3);
        captureLayout = (RelativeLayout) findViewById(R.id.capture_lay);

        captureLayout.setVisibility(View.GONE);

        videoLayout = (RelativeLayout) findViewById(R.id.video_lay);
        generalLayout = (RelativeLayout) findViewById(R.id.general_lay);


        hdrSwitch = (SwitchButton) findViewById(R.id.slidebutoon);

        resolution = (TextView) findViewById(R.id.textView7);
        quality = (TextView) findViewById(R.id.textView9);
        noiseSwitch = (SwitchButton) findViewById(R.id.slidebutoonnoise);
        rootlay = (RelativeLayout) findViewById(R.id.root_lay_rel);
        microphoneSwitch = (SwitchButton) findViewById(R.id.microphoneswitch);


        slideButtonFace = (SwitchButton) findViewById(R.id.slidebutoonface);
        slideButtonSmile = (SwitchButton) findViewById(R.id.slidebutoonSmile);
        storageLocation = (TextView) findViewById(R.id.textView2);
        touchToFocusSwitch = (SwitchButton) findViewById(R.id.slidebutoontouch);
        slideButtonShutter= (SwitchButton) findViewById(R.id.slidebutoonShutter);
        slideButtonQR = (SwitchButton) findViewById(R.id.slidebutoonQR);
        slidebaby = (SwitchButton) findViewById(R.id.slidebutoonBaby);

        blue1 = findViewById(R.id.blueline1);
        blue2 = findViewById(R.id.blueline2);
        blue3 = findViewById(R.id.blueline3);
    }
    //capture methods

    public void init(){

        generalLayout = (RelativeLayout) findViewById(R.id.general_lay);
        captureLayout = (RelativeLayout) findViewById(R.id.capture_lay);

        mGeneral_Layout_Container=(RelativeLayout)findViewById(R.id.general_lay);

        settingWhiteBal = inflater.inflate(R.layout.settingwb, mGeneral_Layout_Container);

        mWhiteBalRadioGroup= (RadioGroup)settingWhiteBal.findViewById(R.id.radiogroupWhite);

        mFaceDetection= (TextView) findViewById(R.id.faceDetectionID);
        mSmileShot= (TextView) findViewById(R.id.smileShotID);

        mSelfTimerView= (TextView) findViewById(R.id.selftimerID);
        mPictureSizeView= (TextView) findViewById(R.id.picturesizeID);
        mPictureQuality= (TextView) findViewById(R.id.pictureQualityID);
        mIso= (TextView) findViewById(R.id.isoID);
        mHDROnOff= (TextView) findViewById(R.id.hdrID);


        mGpsTxtView= (TextView) findViewById(R.id.gpsLocationID);

        mExposureTxtView= (TextView) findViewById(R.id.exposureId);

        mColorEffectTxtView= (TextView) findViewById(R.id.colorEffectID);
        mSceneModeTxtView= (TextView) findViewById(R.id.sceneModeID);
        mWhiteBalTxtView= (TextView) findViewById(R.id.whiteBalID);
        mAntiFlickerTxtView= (TextView) findViewById(R.id.antiflickerID);
        //DefaultRestoreTxtView= (TextView) findViewById(R.id.defaultRestoreID);

        mExposureTxtView.setVisibility(View.VISIBLE);

        mWhiteBalTxtView.setVisibility(View.VISIBLE);

        capture = (ImageView) findViewById(R.id.imageView);
        general= (ImageView) findViewById(R.id.imageView3);
        mGeneral_Layout_Container.setVisibility(View.INVISIBLE);
        blue1 = findViewById(R.id.blueline1);
        blue2 = findViewById(R.id.blueline2);
        blue3 = findViewById(R.id.blueline3);

        blue1.setVisibility(View.VISIBLE);
        blue3.setVisibility(View.INVISIBLE);
        blue2.setVisibility(View.INVISIBLE);
        generalLayout.setVisibility(View.INVISIBLE);
    }
    public  void initializeColorEffect(){
        flagColor=1;
        initilizaSettingView(mGeneral_Layout_Container);

        final ArrayList<String> mColorEffectList=new ArrayList<>();
        mColorEffectList.add(0,"Posterize");
        mColorEffectList.add(1,"Aqua");
        mColorEffectList.add(2,"None");
        mColorEffectList.add(3,"Mono");
        mColorEffectList.add(4,"Negative");
        mColorEffectList.add(5,"Solarize");
        mColorEffectList.add(6,"BlackBoard");
        mColorEffectList.add(7,"WhiteBoard");
        mColorEffectList.add(8,"Sepia");

        for (int i = 0; i < mColorEffectList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(mColorEffectList.get(i));
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_ColorEffect.getInt("coloreffect", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (mColorEffectList.get(0) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_POSTERIZE);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton1);
                        }

                        break;
                    case R.id.radioButton2:
                        if (mColorEffectList.get(1) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_AQUA);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton2);
                        }
                        break;
                    case R.id.radioButton3:
                        if (mColorEffectList.get(2) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_NONE);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton3);
                        }
                        break;
                    case R.id.radioButton4:
                        if (mColorEffectList.get(3) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_MONO);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton4);
                        }
                        break;
                    case R.id.radioButton5:
                        if (mColorEffectList.get(4) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_NEGATIVE);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton5);
                        }
                        break;
                    case R.id.radioButton6:
                        if (mColorEffectList.get(5) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_SOLARIZE);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton6);
                        }
                        break;
                    case R.id.radioButton7:
                        if (mColorEffectList.get(6) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_BLACKBOARD);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton7);
                        }
                        break;
                    case R.id.radioButton8:
                        if (mColorEffectList.get(7) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_WHITEBOARD);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton8);
                        }
                        break;
                    case R.id.radioButton9:
                        if (mColorEffectList.get(8) != null) {
                            mCameraParametrs.setColorEffect(Camera.Parameters.EFFECT_SEPIA);
                            myapp.addToColorPreferences("coloreffect", R.id.radioButton9);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);

            }
        });

    }

    public void initializeSceneModes(){
        flagScene=0;
        initilizaSettingView(mGeneral_Layout_Container);

        final ArrayList<String> mSceneModeList=new ArrayList<>();
        mSceneModeList.add(0, "Auto");
        mSceneModeList.add(1,"Night");
        mSceneModeList.add(2,"Sunset");
        mSceneModeList.add(3, "Party");
        mSceneModeList.add(4, "Portrait");
        mSceneModeList.add(5, "Landscape");
        mSceneModeList.add(6,"Night portrait");
        mSceneModeList.add(7,"Theater");
        mSceneModeList.add(8,"Beach");
        mSceneModeList.add(9,"Snow");
        mSceneModeList.add(10,"Steady photo");
        mSceneModeList.add(11,"Fireworks");
        mSceneModeList.add(12,"Sports");
        mSceneModeList.add(13,"Candle light");

        for (int i = 0; i < mSceneModeList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(mSceneModeList.get(i));
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_ColorEffect.getInt("SceneMode", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (mSceneModeList.get(0) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton1);
                        }

                        break;
                    case R.id.radioButton2:
                        if (mSceneModeList.get(1) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_NIGHT);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton2);
                        }
                        break;
                    case R.id.radioButton3:
                        if (mSceneModeList.get(2) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_SUNSET);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton3);
                        }
                        break;
                    case R.id.radioButton4:
                        if (mSceneModeList.get(3) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_PARTY);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton4);
                        }
                        break;
                    case R.id.radioButton5:
                        if (mSceneModeList.get(4) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton5);
                        }
                        break;
                    case R.id.radioButton6:
                        if (mSceneModeList.get(5) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_LANDSCAPE);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton6);
                        }
                        break;
                    case R.id.radioButton7:
                        if (mSceneModeList.get(6) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_NIGHT_PORTRAIT);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton7);
                        }
                        break;
                    case R.id.radioButton8:
                        if (mSceneModeList.get(7) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_THEATRE);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton8);
                        }
                        break;
                    case R.id.radioButton9:
                        if (mSceneModeList.get(8) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_BEACH);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton9);
                        }
                        break;
                    case R.id.radioButton10:
                        if (mSceneModeList.get(9) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_SNOW);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton10);
                        }
                        break;
                    case R.id.radioButton11:
                        if (mSceneModeList.get(10) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_STEADYPHOTO);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton11);
                        }
                        break;
                    case R.id.radioButton12:
                        if (mSceneModeList.get(11) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_FIREWORKS);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton12);
                        }
                        break;
                    case R.id.radioButton13:
                        if (mSceneModeList.get(12) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_SPORTS);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton13);
                        }
                        break;
                    case R.id.radioButton14:
                        if (mSceneModeList.get(13) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_CANDLELIGHT);
                            myapp.addToSceneModePreferences("SceneMode", R.id.radioButton14);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);

            }
        });

    }

    public void initializeWhiteBalView(){
        //   String[] StaringWhiteBalData={"Auto","Incandescent","Daylight","Fluorescent","Cloudy","Twilight","Shade","Warm Fluorescent"};
        mWhiteBalRadioGroup.check(myapp.ChackedStatusPreference.getInt("state", 0));

        flagWhiteBal=1;

        initilizaSettingView(mGeneral_Layout_Container);
        final ArrayList<String> WhiteBalancList=new ArrayList<>();
        WhiteBalancList.add(0,"Auto");
        WhiteBalancList.add(1,"Incandescent");
        WhiteBalancList.add(2, "Daylight");
        WhiteBalancList.add(3,"Fluorescent");
        WhiteBalancList.add(4,"Cloudy");
        WhiteBalancList.add(5,"Twilight");
        WhiteBalancList.add(6, "Shade");
        WhiteBalancList.add(7,"Warm fluorescent");

        for (int i = 0; i < WhiteBalancList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(WhiteBalancList.get(i));
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_ColorEffect.getInt("whitebalance", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (WhiteBalancList.get(0) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton1);
                        }

                        break;
                    case R.id.radioButton2:
                        if (WhiteBalancList.get(1) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_INCANDESCENT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton2);
                        }
                        break;
                    case R.id.radioButton3:
                        if (WhiteBalancList.get(2) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_DAYLIGHT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton3);
                        }
                        break;

                    case R.id.radioButton4:
                        if (WhiteBalancList.get(3) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_FLUORESCENT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton4);
                        }
                        break;
                    case R.id.radioButton5:
                        if (WhiteBalancList.get(4) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_CLOUDY_DAYLIGHT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton5);
                        }
                        break;
                    case R.id.radioButton6:
                        if (WhiteBalancList.get(5) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_TWILIGHT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton6);
                        }
                        break;
                    case R.id.radioButton7:
                        if (WhiteBalancList.get(6) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_SHADE);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton7);
                        }
                        break;
                    case R.id.radioButton8:
                        if (WhiteBalancList.get(7) != null) {
                            mCameraParametrs.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_WARM_FLUORESCENT);
                            myapp.addToCheckedPreferances("whitebalance", R.id.radioButton8);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);

            }
        });


    }
    public void initializeExposureView()
    {
        flagExposure=1;
        final int num;
        initilizaSettingView(mGeneral_Layout_Container);

        num=mCameraParametrs.getMaxExposureCompensation();
        Log.e("MaxExposure:", "Max Exposure Value:" + num);

        mElementList=new ArrayList<>();

        int number=0;
        int counter=0;
        for (int i=0;i<=num;i++){

            mElementList.add(i, number);
            number--;
            counter++;
        }
        Collections.reverse(mElementList);

        int temp=num;
        for (int j=1;j<=num;j++)
        {
            temp++;
            mElementList.add(temp, j);
        }
        for (int i=0;i<mElementList.size();i++){
            // Log.e("first title:","title:"+mElementList.get(i));
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            String value=mElementList.get(i).toString();
            radiobutton.get(i).setText("      "+value);
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_Exposure.getInt("exposure", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:

                        if (mElementList.get(0)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(0));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton1);
                        }

                        break;
                    case R.id.radioButton2:
                        if (mElementList.get(1)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(1));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton2);
                        }
                        break;
                    case R.id.radioButton3:
                        if (mElementList.get(2)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(2));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton3);
                        }
                        break;
                    case R.id.radioButton4:
                        if (mElementList.get(3)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(3));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton4);
                        }
                        break;
                    case R.id.radioButton5:
                        if (mElementList.get(4)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(4));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton5);
                        }
                        break;
                    case R.id.radioButton6:
                        if (mElementList.get(5)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(5));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton6);
                        }
                        break;
                    case R.id.radioButton7:
                        if (mElementList.get(6)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(6));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton7);
                        }
                        break;
                    case R.id.radioButton8:
                        if (mElementList.get(7)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(7));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton8);
                        }
                        break;
                    case R.id.radioButton9:
                        if (mElementList.get(8)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(8));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton9);
                        }
                        break;
                    case R.id.radioButton10:
                        if (mElementList.get(9)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(9));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton10);
                        }
                        break;
                    case R.id.radioButton11:
                        if (mElementList.get(10)!=null){
                            mCameraParametrs.setExposureCompensation(mElementList.get(10));
                            myapp.addToExposurePreferences("exposure", R.id.radioButton11);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);
            }
        });
    }
    public void initializePIctureSize_elite2() {
        settingPictureSize_elite2=inflater.inflate(R.layout.settingpic_sizelayout_elite2,captureLayout);
        mPictureLinearLayout_elite2= (LinearLayout)settingPictureSize_elite2.findViewById(R.id.pictureSizeLinLayout_elite2);
        mPictureSizeRadioGroup_elite2= (RadioGroup)settingPictureSize_elite2.findViewById(R.id.radiogroupPictureSize_elite2);

        b1= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonQVGA_elite2);
        b2= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonVGA_elite2);
        b3= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonOneMega_elite2);
        b4= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonTwoMega_elite2);
        b5= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonThreeMega_elite2);
        b6= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonFourMega_elite2);
        b7= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonFiveMega_elite2);
        b8= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonSixMega_elite2);
        b9= (RadioButton) settingPictureSize_elite2.findViewById(R.id.radioButtonSevanMega_elite2);

        radiobutton=new ArrayList<>();

        radiobutton.add(0,b1);
        radiobutton.add(1,b2);
        radiobutton.add(2,b3);
        radiobutton.add(3,b4);
        radiobutton.add(4,b5);
        radiobutton.add(5,b6);
        radiobutton.add(6,b7);
        radiobutton.add(7, b8);
        radiobutton.add(8,b9);
       /* radiobutton.add(11,b12);
        radiobutton.add(12,b13);*/
        initLineView(captureLayout);

        mPictureSizeValues=new ArrayList<Integer>();

        mPictureSizeValues.add(0,R.id.radioButtonQVGA_elite2);
        mPictureSizeValues.add(1,R.id.radioButtonVGA_elite2);
        mPictureSizeValues.add(2,R.id.radioButtonOneMega_elite2);
        mPictureSizeValues.add(3,R.id.radioButtonTwoMega_elite2);
        mPictureSizeValues.add(4,R.id.radioButtonThreeMega_elite2);
        mPictureSizeValues.add(5,R.id.radioButtonFourMega_elite2);
        mPictureSizeValues.add(6,R.id.radioButtonFiveMega_elite2);
        mPictureSizeValues.add(7,R.id.radioButtonFiveMega_elite2);
        mPictureSizeValues.add(8,R.id.radioButtonSixMega_elite2);
        mPictureSizeValues.add(9,R.id.radioButtonSevanMega_elite2);

    }
    public void initializePictureSizeView(){

        settingPictureSize=inflater.inflate(R.layout.settingpicturesize,captureLayout);
        mPictureLinearLayout= (LinearLayout)settingPictureSize.findViewById(R.id.pictureSizeLinLayout);
        mPictureSizeRadioGroup= (RadioGroup)settingPictureSize.findViewById(R.id.radiogroupPictureSize);

        b1= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonQVGA);
        b2= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonVGA);
        b3= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonOneMega);
        b4= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonTwoMega);
        b5= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonThreeMega);
        b6= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonFourMega);
        b7= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonFiveMega);
        b8= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonSixMega);
        b9= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonSevanMega);
        b10= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonEightMega);
        b11= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonNineMega);
       /* b12= (RadioButton) settingPictureSize.findViewById(R.id.radioButtontenMega);
        b13= (RadioButton) settingPictureSize.findViewById(R.id.radioButtonelevanMega);*/

        radiobutton=new ArrayList<>();

        radiobutton.add(0,b1);
        radiobutton.add(1,b2);
        radiobutton.add(2,b3);
        radiobutton.add(3,b4);
        radiobutton.add(4,b5);
        radiobutton.add(5,b6);
        radiobutton.add(6,b7);
        radiobutton.add(7,b8);
        radiobutton.add(8,b9);
        radiobutton.add(9,b10);
        radiobutton.add(10,b11);
       /* radiobutton.add(11,b12);
        radiobutton.add(12,b13);*/
        initLineView(captureLayout);

        mPictureSizeValues=new ArrayList<Integer>();

        mPictureSizeValues.add(0, R.id.radioButtonQVGA);
        mPictureSizeValues.add(1,R.id.radioButtonVGA);
        mPictureSizeValues.add(2,R.id.radioButtonOneMega);
        mPictureSizeValues.add(3,R.id.radioButtonTwoMega);
        mPictureSizeValues.add(4,R.id.radioButtonThreeMega);
        mPictureSizeValues.add(5,R.id.radioButtonFourMega);
        mPictureSizeValues.add(6,R.id.radioButtonFiveMega);
        mPictureSizeValues.add(7,R.id.radioButtonFiveMega);
        mPictureSizeValues.add(8,R.id.radioButtonSixMega);
        mPictureSizeValues.add(9,R.id.radioButtonSevanMega);
        mPictureSizeValues.add(10,R.id.radioButtonEightMega);
        mPictureSizeValues.add(11,R.id.radioButtonNineMega);

        //   mPictureLinearLayout.addView(rg);

    }
    public  void initializePicQualityView(){
        flagPictureQuality=1;
        initilizaSettingView(captureLayout);



        if (mElementList!=null) {
            for (int i = 0; i < mElementList.size(); i++) {
                radiobutton.get(i).setVisibility(View.GONE);
            }
        }


        final ArrayList<String> mPicQuality_List=new ArrayList<>();

        mPicQuality_List.add(0,"Normal");
        mPicQuality_List.add(1,"Fine");
        mPicQuality_List.add(2, "Superfine");

        for (int i=0;i<3;i++){
            // Log.e("first title:","title:"+mElementList.get(i));
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            String value=mPicQuality_List.get(i).toString();

            radiobutton.get(i).setText("      " + value);
            radiobutton.get(i).setTextSize(14);
            radiobutton.get(i).setCompoundDrawablesWithIntrinsicBounds(0, 0, android.support.v7.appcompat.R.drawable.abc_btn_radio_material, 0);
            radiobutton.get(i).setPadding(0, 7, 0, 7);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_picQuality.getInt("picquality",0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        mCameraParametrs.setJpegQuality(65);
                        myapp.addToPicQualityPreferences("picquality", R.id.radioButton1);
                        mPictureQualityValue.setText(mPicQuality_List.get(0).toString());
                            myapp.addToStringValuePreferances("PICTUREQUALITY_STR",mPicQuality_List.get(0).toString());

                        break;
                    case R.id.radioButton2:
                        mCameraParametrs.setJpegQuality(75);
                        myapp.addToPicQualityPreferences("picquality", R.id.radioButton2);
                        mPictureQualityValue.setText(mPicQuality_List.get(1).toString());
                        myapp.addToStringValuePreferances("PICTUREQUALITY_STR", mPicQuality_List.get(1).toString());
                        break;
                    case R.id.radioButton3:
                        mCameraParametrs.setJpegQuality(100);
                        myapp.addToPicQualityPreferences("picquality", R.id.radioButton3);
                        mPictureQualityValue.setText(mPicQuality_List.get(2).toString());
                        myapp.addToStringValuePreferances("PICTUREQUALITY_STR", mPicQuality_List.get(2).toString());
                        break;
                }
                mPictureQualityValue.setTextColor(Color.WHITE);
                myapp.mCamera.setParameters(mCameraParametrs);

            }
        });

    }
    public void initilizeIsoView()
    {
        flagIso=1;
        initilizaSettingView(captureLayout);


        String supportedIsoValues=mCameraParametrs.get("iso-speed-values");
        String[] strinArr={supportedIsoValues};
        final List<String> SupportedIsoArr= Arrays.asList(supportedIsoValues.split(","));
        final ArrayList<String> mArralist_IsoElemnt=new ArrayList<>();
        int i=0;
        for (String e: SupportedIsoArr){
            mArralist_IsoElemnt.add(i,e);
            i++;
        }
        String[] ISOArry=mArralist_IsoElemnt.toArray(new String[mArralist_IsoElemnt.size()]);

        int len=mArralist_IsoElemnt.size();
        for (int j=0;j<mArralist_IsoElemnt.size();j++){

            radiobutton.get(j).setVisibility(View.VISIBLE);
            mViewsList.get(j).setVisibility(View.VISIBLE);
            String value=mArralist_IsoElemnt.get(j).toString();
            radiobutton.get(j).setText("      " + value);
            radiobutton.get(j).setTextSize(14);
            radiobutton.get(i).setCompoundDrawablesWithIntrinsicBounds(0,0,android.support.v7.appcompat.R.drawable.abc_btn_radio_material,0);
            radiobutton.get(i).setPadding(0, 7, 0, 7);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_Iso.getInt("iso",0));
        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {
                    case R.id.radioButton1:

                        if (mArralist_IsoElemnt.get(0)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(0));
                            myapp.addToIsoPreferences("iso", R.id.radioButton1);
                            Log.e("Test:", "Test 0");
                            mIsoValue.setText(mArralist_IsoElemnt.get(0));
                            myapp.addToStringValuePreferances("ISO_STR",mArralist_IsoElemnt.get(0).toString());
                        }
                        break;
                    case R.id.radioButton2:
                        if (mArralist_IsoElemnt.get(1)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(1));
                            myapp.addToIsoPreferences("iso", R.id.radioButton2);
                            Log.e("Test:", "Test 1");
                            mIsoValue.setText(mArralist_IsoElemnt.get(1));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(1).toString());
                        }
                        break;
                    case R.id.radioButton3:
                        if (mArralist_IsoElemnt.get(2)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(2));
                            myapp.addToIsoPreferences("iso", R.id.radioButton3);
                            Log.e("Test:", "Test 2");
                            mIsoValue.setText(mArralist_IsoElemnt.get(2));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(2).toString());
                        }
                        break;
                    case R.id.radioButton4:
                        if (mArralist_IsoElemnt.get(3)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(3));
                            myapp.addToIsoPreferences("iso", R.id.radioButton4);
                            Log.e("Test:", "Test 3");
                            mIsoValue.setText(mArralist_IsoElemnt.get(3));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(3).toString());

                        }
                        break;
                    case R.id.radioButton5:
                        if (mArralist_IsoElemnt.get(4)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(4));
                            myapp.addToIsoPreferences("iso", R.id.radioButton5);
                            mIsoValue.setText(mArralist_IsoElemnt.get(4));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(4).toString());
                            Log.e("Test:", "Test 4");
                        }
                        break;
                    case R.id.radioButton6:
                        if (mArralist_IsoElemnt.get(5)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(5));
                            myapp.addToIsoPreferences("iso", R.id.radioButton6);
                            Log.e("Test:", "Test 5");
                            mIsoValue.setText(mArralist_IsoElemnt.get(5));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(5).toString());
                        }
                        break;
                    case R.id.radioButton7:
                        if (mArralist_IsoElemnt.get(6)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(6));
                            myapp.addToIsoPreferences("iso", R.id.radioButton7);
                            mIsoValue.setText(mArralist_IsoElemnt.get(6));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(6).toString());
                        }
                        break;
                    case R.id.radioButton8:
                        if (mArralist_IsoElemnt.get(7)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(7));
                            myapp.addToIsoPreferences("iso", R.id.radioButton8);
                            mIsoValue.setText(mArralist_IsoElemnt.get(7));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(7).toString());
                        }
                        break;
                    case R.id.radioButton9:
                        if (mArralist_IsoElemnt.get(8)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(8));
                            myapp.addToIsoPreferences("iso", R.id.radioButton9);
                            mIsoValue.setText(mArralist_IsoElemnt.get(8));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(8).toString());
                        }
                        break;
                    case R.id.radioButton10:
                        if (mArralist_IsoElemnt.get(9)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(9));
                            myapp.addToIsoPreferences("iso", R.id.radioButton10);
                            mIsoValue.setText(mArralist_IsoElemnt.get(9));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(9).toString());
                        }
                        break;
                    case R.id.radioButton11:
                        if (mArralist_IsoElemnt.get(10)!=null){
                            mCameraParametrs.set("iso", (String) mArralist_IsoElemnt.get(10));
                            myapp.addToIsoPreferences("iso", R.id.radioButton11);
                            mIsoValue.setText(mArralist_IsoElemnt.get(10));
                            myapp.addToStringValuePreferances("ISO_STR", mArralist_IsoElemnt.get(10).toString());
                        }
                        break;
                }
                mIsoValue.setTextColor(Color.WHITE);
                myapp.mCamera.setParameters(mCameraParametrs);
            }
        });
    }
    public void initHdrView(){
        initilizaSettingView(captureLayout);

        final ArrayList<String> AntiFlickerList=new ArrayList<>();
        AntiFlickerList.add(0,"ON");
        AntiFlickerList.add(1,"OFF");
        for (int i = 0; i < AntiFlickerList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(AntiFlickerList.get(i));
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_AntiFlicker.getInt("hdr", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (AntiFlickerList.get(0) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
                            myapp.addToHdrModePreferences("hdr", R.id.radioButton1);
                        }
                        break;
                    case R.id.radioButton2:
                        if (AntiFlickerList.get(1) != null) {
                            mCameraParametrs.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                            myapp.addToHdrModePreferences("hdr", R.id.radioButton2);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);
            }
        });
    }
    public void initializeAntiFlickerView(){
        initilizaSettingView(mGeneral_Layout_Container);
        flagAntiflicker=1;
        final ArrayList<String> AntiFlickerList=new ArrayList<>();
        AntiFlickerList.add(0,"Auto");
        AntiFlickerList.add(1, "OFF");
        AntiFlickerList.add(2,"50Hz");
        AntiFlickerList.add(3,"60Hz");

        for (int i = 0; i < AntiFlickerList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(AntiFlickerList.get(i));
            radiobutton.get(i).setTextSize(14);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_AntiFlicker.getInt("flicker", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (AntiFlickerList.get(0) != null) {
                            mCameraParametrs.setAntibanding(Camera.Parameters.ANTIBANDING_AUTO);
                            myapp.addToFlickerModePreferences("flicker", R.id.radioButton1);
                        }
                        break;
                    case R.id.radioButton2:
                        if (AntiFlickerList.get(1) != null) {
                            mCameraParametrs.setAntibanding(Camera.Parameters.ANTIBANDING_OFF);
                            myapp.addToFlickerModePreferences("flicker", R.id.radioButton2);
                        }
                        break;
                    case R.id.radioButton3:
                        if (AntiFlickerList.get(2) != null) {
                            mCameraParametrs.setAntibanding(Camera.Parameters.ANTIBANDING_50HZ);
                            myapp.addToFlickerModePreferences("flicker", R.id.radioButton3);
                        }
                        break;
                    case R.id.radioButton4:
                        if (AntiFlickerList.get(3) != null) {
                            mCameraParametrs.setAntibanding(Camera.Parameters.ANTIBANDING_60HZ);
                            myapp.addToFlickerModePreferences("flicker", R.id.radioButton4);
                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);
            }
        });
    }
    public void initializeSelfTimer(){
        flagSelfTimer=1;
        initilizaSettingView(captureLayout);
        mTimerValue= (ImageView) findViewById(R.id.imgTimerValue);

        final ArrayList<String> TimerList=new ArrayList<>();

        TimerList.add(0,"OFF");
        TimerList.add(1,"2 Seconds");
        TimerList.add(2,"5 Seconds");
        TimerList.add(3,"10 Seconds");

        int[] arrList ={R.mipmap.time_off,R.mipmap.two_second,R.mipmap.five_second,R.mipmap.ten_second};

        for (int i = 0; i < TimerList.size(); i++) {
            radiobutton.get(i).setVisibility(View.VISIBLE);
            mViewsList.get(i).setVisibility(View.VISIBLE);
            radiobutton.get(i).setText(TimerList.get(i));
            radiobutton.get(i).setCompoundDrawablesWithIntrinsicBounds(arrList[i], 0, android.support.v7.appcompat.R.drawable.abc_btn_radio_material, 0);
            radiobutton.get(i).setTextSize(14);
            radiobutton.get(i).setPadding(0, 7, 0, 7);
        }
        mSettingRadioGroup.check(myapp.sharedPreferences_SelfTimer.getInt("timer", 0));

        mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch (radioGroup.getCheckedRadioButtonId()) {

                    case R.id.radioButton1:
                        if (TimerList.get(0) != null) {
                            myapp.setSelfTimerInput(0);
                            myapp.mCountDownTimer=false;
                            myapp.addToSelfTimerPreferences("timer", R.id.radioButton1);
                            mTimerValue.setImageResource(R.mipmap.time_off);
                        }
                        break;
                    case R.id.radioButton2:
                        if (TimerList.get(1) != null) {
                            myapp.setSelfTimerInput(3000);
                            myapp.mCountDownTimer=true;
                            myapp.addToSelfTimerPreferences("flicker", R.id.radioButton2);
                            mTimerValue.setImageResource(R.mipmap.two_second);
                        }

                        break;
                    case R.id.radioButton3:
                        if (TimerList.get(2) != null) {
                            myapp.setSelfTimerInput(6000);
                            myapp.mCountDownTimer=true;
                            myapp.addToSelfTimerPreferences("timer", R.id.radioButton3);
                            mTimerValue.setImageResource(R.mipmap.five_second);

                        }

                        break;
                    case R.id.radioButton4:
                        if (TimerList.get(3) != null) {
                            myapp.setSelfTimerInput(11000);
                            myapp.mCountDownTimer=true;
                            myapp.addToSelfTimerPreferences("timer", R.id.radioButton4);
                            mTimerValue.setImageResource(R.mipmap.ten_second);

                        }
                        break;
                }
                myapp.mCamera.setParameters(mCameraParametrs);
                mSettingRelativeLayout.setVisibility(View.INVISIBLE);
                flagSelfTimerSRadio=1;
                flagSelfTimer=0;
                mSelfTimerView.setTextColor(Color.WHITE);

            }
        });

    }

    public void getPictureSize(){
        Camera.Parameters CameraParametrs=myapp.mCamera.getParameters();
        List<Camera.Size> sizes = CameraParametrs.getSupportedPictureSizes();
        mList=new LinkedHashMap<>();

        double widthPixel,heightPixel,resultantPixels;
        Camera.Size mSize = null;
        for (int i = 0; i < sizes.size(); i++) {
            Log.e("myapp.mCamera.Size:", "Width =" + sizes.get(i).width + "Height==" + sizes.get(i).height);
            widthPixel=sizes.get(i).width;
            heightPixel=sizes.get(i).height;
            resultantPixels=myapp.getMegapixels(widthPixel,heightPixel);

            float meapixels = (float)resultantPixels;
            if (meapixels<=0.3){
                QVGA=(int)meapixels;
                mList.put(100, sizes.get(i));
                Log.e("QGVA:","QGVA:"+meapixels);
            }
            else if (meapixels<=0.7&& meapixels>0.3){
                VGA=(int)meapixels;
                mList.put(200, sizes.get(i));
                Log.e("GVA:", "GVA:" + meapixels);
            }
            else if(meapixels>=0.8&&meapixels<=1.4){
                mList.put(1, sizes.get(i));
                Log.e(">0.7 ", "executed");
            }
            else if (meapixels>=1.7&&meapixels<=2.4){
                mList.put(2, sizes.get(i));
                Log.e(">1.7 ", "executed");
            }
            else if (meapixels>=2.7&&meapixels<=3.4){
                mList.put(3, sizes.get(i));
                Log.e(">2.7 ", "executed");
            }
            else if (meapixels>=3.5&&meapixels<=4.6){
                mList.put(4, sizes.get(i));
                Log.e(">3.7 ", "executed");
            }
            else if (meapixels>=4.7&&meapixels<=5.4){
                mList.put(5, sizes.get(i));
            }
            else if (meapixels>=5.7&&meapixels<=6.5){
                mList.put(6, sizes.get(i));
            }
            else if (meapixels>6.5&&meapixels<=7.7){
                mList.put(7, sizes.get(i));
            }
            else if (meapixels>=7.8&&meapixels<=8.4){
                mList.put(8, sizes.get(i));
            }
            else if (meapixels>=8.7&&meapixels<=9.4){
                mList.put(9, sizes.get(i));
            }
            else if (meapixels>=9.7&&meapixels<=10.4){
                mList.put(10, sizes.get(i));
            }
            else if (meapixels>=10.7&&meapixels<=11.4){
                mList.put(11, sizes.get(i));
            }
            else if (meapixels>=11.7&&meapixels<=12.4){
                mList.put(12, sizes.get(i));
            }
            else if (meapixels>=12.7&&meapixels<=13.4){
                mList.put(13,sizes.get(i));
            }
        }

    }
    public void initilizaSettingView(RelativeLayout layout){
       BackPressedVal=0;
        mSettingLayout=inflater.inflate(R.layout.setting_exposure_layout,layout);
        mLineraLayout= (LinearLayout)mSettingLayout.findViewById(R.id.settingLinLayout);
        mSettingRadioGroup=(RadioGroup)mSettingLayout.findViewById(R.id.RadioGroupID);
        mSettingRelativeLayout=(RelativeLayout)mSettingLayout.findViewById(R.id.SettingRelLayout);
        mSettingRelativeLayout.setVisibility(View.VISIBLE);

        b1= (RadioButton) mSettingLayout.findViewById(R.id.radioButton1);
        b2= (RadioButton) mSettingLayout.findViewById(R.id.radioButton2);
        b3= (RadioButton) mSettingLayout.findViewById(R.id.radioButton3);
        b4= (RadioButton) mSettingLayout.findViewById(R.id.radioButton4);
        b5= (RadioButton) mSettingLayout.findViewById(R.id.radioButton5);
        b6= (RadioButton) mSettingLayout.findViewById(R.id.radioButton6);
        b7= (RadioButton) mSettingLayout.findViewById(R.id.radioButton7);
        b8= (RadioButton) mSettingLayout.findViewById(R.id.radioButton8);
        b9= (RadioButton) mSettingLayout.findViewById(R.id.radioButton9);
        b10= (RadioButton) mSettingLayout.findViewById(R.id.radioButton10);
        b11= (RadioButton) mSettingLayout.findViewById(R.id.radioButton11);
        b12= (RadioButton) mSettingLayout.findViewById(R.id.radioButton12);
        b13= (RadioButton) mSettingLayout.findViewById(R.id.radioButton13);
        b14= (RadioButton) mSettingLayout.findViewById(R.id.radioButton14);

        radiobutton=new ArrayList<>();

        radiobutton.add(0,b1);
        radiobutton.add(1,b2);
        radiobutton.add(2,b3);
        radiobutton.add(3,b4);
        radiobutton.add(4,b5);
        radiobutton.add(5,b6);
        radiobutton.add(6,b7);
        radiobutton.add(7,b8);
        radiobutton.add(8,b9);
        radiobutton.add(9,b10);
        radiobutton.add(10,b11);
        radiobutton.add(11,b12);
        radiobutton.add(12,b13);
        radiobutton.add(13,b14);

        initLineView(layout);

        mPictureSizeValues=new ArrayList<Integer>();

        mPictureSizeValues.add(0,R.id.radioButton1);
        mPictureSizeValues.add(1,R.id.radioButton2);
        mPictureSizeValues.add(2,R.id.radioButton3);
        mPictureSizeValues.add(3,R.id.radioButton4);
        mPictureSizeValues.add(4,R.id.radioButton5);
        mPictureSizeValues.add(5,R.id.radioButton6);
        mPictureSizeValues.add(6,R.id.radioButton7);
        mPictureSizeValues.add(7,R.id.radioButton8);
        mPictureSizeValues.add(8,R.id.radioButton9);
        mPictureSizeValues.add(9,R.id.radioButton10);
        mPictureSizeValues.add(10,R.id.radioButton11);
        mPictureSizeValues.add(11,R.id.radioButton12);
        mPictureSizeValues.add(12,R.id.radioButton13);
        mPictureSizeValues.add(13,R.id.radioButton14);

    }
    public void initLineView(RelativeLayout layout){
        mSettingLayout=inflater.inflate(R.layout.setting_exposure_layout,layout);
        mLineraLayout= (LinearLayout)mSettingLayout.findViewById(R.id.settingLinLayout);
        mSettingRadioGroup=(RadioGroup)mSettingLayout.findViewById(R.id.RadioGroupID);
        mSettingRelativeLayout=(RelativeLayout)mSettingLayout.findViewById(R.id.SettingRelLayout);
        mSettingRelativeLayout.setVisibility(View.VISIBLE);

        v1=(View)mSettingLayout.findViewById(R.id.radioview1);
        v2=(View)mSettingLayout.findViewById(R.id.radioview2);
        v3=(View)mSettingLayout.findViewById(R.id.radioview3);
        v4=(View)mSettingLayout.findViewById(R.id.radioview4);
        v5=(View)mSettingLayout.findViewById(R.id.radioview5);
        v6=(View)mSettingLayout.findViewById(R.id.radioview6);
        v7=(View)mSettingLayout.findViewById(R.id.radioview7);
        v8=(View)mSettingLayout.findViewById(R.id.radioview8);
        v9=(View)mSettingLayout.findViewById(R.id.radioview9);
        v10=(View)mSettingLayout.findViewById(R.id.radioview10);
        v11=(View)mSettingLayout.findViewById(R.id.radioview11);
        v12=(View)mSettingLayout.findViewById(R.id.radioview12);
        v13=(View)mSettingLayout.findViewById(R.id.radioview13);
        v14=(View)mSettingLayout.findViewById(R.id.radioview14);

        mViewsList=new ArrayList<>();

        mViewsList.add(0,v1);
        mViewsList.add(1,v2);
        mViewsList.add(2,v3);
        mViewsList.add(3,v4);
        mViewsList.add(4,v5);
        mViewsList.add(5,v6);
        mViewsList.add(6,v7);
        mViewsList.add(7,v8);
        mViewsList.add(8,v9);
        mViewsList.add(9,v10);
        mViewsList.add(10,v11);
        mViewsList.add(11,v12);
        mViewsList.add(12,v13);
        mViewsList.add(13,v14);
    }
    public void onBackPressed() {
        // super.onBackPressed();
        if (mSettingRelativeLayout!=null&& BackPressedVal==0){
            mSettingRelativeLayout.setVisibility(View.INVISIBLE);
            backMethod();
            BackPressedVal=1;
            flagSelfTimer=2;
            flagWhiteBal=2;
            flagPictureSize=2;
            flagIso=2;
            flagPictureQuality=2;
            flagExposure=2;
            flagScene=2;
            flagAntiflicker=2;
            flagColor=2;

        }else if (BackPressedVal==1){
            backMethod();
            finish();
        }else if (mSettingRelativeLayout==null){
            backMethod();
            finish();
        }
    }
    public  void backMethod(){
        if (mSelfTimerView!=null){
            mSelfTimerView.setTextColor(Color.WHITE);
        }
        if (mPictureQuality!=null){
            mPictureQuality.setTextColor(Color.WHITE);
        }
        if (mIso!=null){
            mIso.setTextColor(Color.WHITE);
        }
        if (mAntiFlickerTxtView!=null){
            mAntiFlickerTxtView.setTextColor(Color.WHITE);
        }
        if (mSceneModeTxtView!=null){
            mSceneModeTxtView.setTextColor(Color.WHITE);
        }
        if (mColorEffectTxtView!=null){
            mColorEffectTxtView.setTextColor(Color.WHITE);
        }
        if (mWhiteBalTxtView!=null){

            mWhiteBalTxtView.setTextColor(Color.WHITE);
        }
        if (mExposureTxtView!=null){
            mExposureTxtView.setTextColor(Color.WHITE);
        }
        if (mPictureSizeView!=null){
            mPictureSizeView.setTextColor(Color.WHITE);
        }
    }
    public void makeViewsNull(RelativeLayout layout){
        backMethod();
        initilizaSettingView(layout);
        for (int j=0;j<14;j++){
            radiobutton.get(j).setVisibility(View.GONE);
            mViewsList.get(j).setVisibility(View.GONE);
        }
    }
    public void pictureSizeMethod()
    {

        flagPictureSize=1;
        getPictureSize();
        initilizaSettingView(captureLayout);
        mPixelList = new ArrayList<>();
        mPixelParamList = new ArrayList<>();

        int n = 0;
        for (Map.Entry<Integer, Camera.Size> entry : mList.entrySet()) {
            Log.e("Map :", "Map Key:" + entry.getKey() + "Map Value:" + entry.getValue());
            mPixelList.add(entry.getKey());
            mPixelParamList.add(n, entry.getValue());
            n++;
        }

        Set<Integer> PixelList = mList.keySet();
        Collections.sort(mPixelList);
        int lenth = mPixelList.size();
        mPixelList.remove(lenth - 1);
        mPixelList.remove(lenth - 2);
        String var = "pixels";

        pixelArrayList = new ArrayList<>();
        for (int i = 0; i < mPixelList.size(); i++) {
            pixelArrayList.add(mPixelList.get(i) + " " + var);
        }
        pixelArrayList.add(0, "QVGA");
        pixelArrayList.add(1, "VGA");

        String[] StrPixelArr = pixelArrayList.toArray(new String[pixelArrayList.size()]);

        if (mPixelParamList.size() <= 9) {

            for (int i = 0; i < pixelArrayList.size(); i++) {
                radiobutton.get(i).setVisibility(View.VISIBLE);
                mViewsList.get(i).setVisibility(View.VISIBLE);
                radiobutton.get(i).setText(pixelArrayList.get(i));
                radiobutton.get(i).setTextSize(14);
                radiobutton.get(i).setCompoundDrawablesWithIntrinsicBounds(0, 0, android.support.v7.appcompat.R.drawable.abc_btn_radio_material, 0);
                radiobutton.get(i).setPadding(0, 7, 0, 7);
            }
                mSettingRadioGroup.check(myapp.PictureSizeStatusPreference_elite2.getInt("picturestate_elite2", 0));



            mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (radioGroup.getCheckedRadioButtonId()) {

                        case R.id.radioButton1:
                            mCameraParametrs.setPictureSize(mList.get(100).width, mList.get(100).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton1);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(0).toString());
                            Log.e("elite2:", "radioButtonQVGA:");
                            mPictureSizeValue.setText(pixelArrayList.get(0).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);

                            break;
                        case R.id.radioButton2:
                            mCameraParametrs.setPictureSize(mList.get(200).width, mList.get(200).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton2);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(1).toString());
                            Log.e("elite2:", "radioButtonVGA:");
                            mPictureSizeValue.setText(pixelArrayList.get(1).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton3:
                            mCameraParametrs.setPictureSize(mList.get(1).width, mList.get(1).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton3);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(2).toString());
                            Log.e("elite2:", "radioButtonOnemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(2).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton4:
                            mCameraParametrs.setPictureSize(mList.get(2).width, mList.get(2).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton4);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(3).toString());
                            Log.e("elite2:", "radioButtonTwoMega:");
                            mPictureSizeValue.setText(pixelArrayList.get(3).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);

                            break;
                        case R.id.radioButton5:
                            mCameraParametrs.setPictureSize(mList.get(3).width, mList.get(3).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton5);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(4).toString());
                            Log.e("elite2:", "radioButtonThreemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(4).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton6:
                            mCameraParametrs.setPictureSize(mList.get(4).width, mList.get(4).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton6);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(5).toString());
                            Log.e("elite2:", "radioButtonFourmega:");
                            mPictureSizeValue.setText(pixelArrayList.get(5).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton7:
                            mCameraParametrs.setPictureSize(mList.get(5).width, mList.get(5).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton7);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(6).toString());
                            Log.e("elite2:", "radioButtonFivemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(6).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton8:
                            mCameraParametrs.setPictureSize(mList.get(6).width, mList.get(6).height);
                            myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton8);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(7).toString());
                            Log.e("elite2:", "radioButtonSixmega:");
                            mPictureSizeValue.setText(pixelArrayList.get(7).toString());
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                        case R.id.radioButton9:
                            if (mList.get(7) != null) {
                                mCameraParametrs.setPictureSize(mList.get(7).width, mList.get(7).height);
                                myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton9);
                                myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(8).toString());
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            } else if (mList.get(8) != null) {
                                mCameraParametrs.setPictureSize(mList.get(8).width, mList.get(8).height);
                                myapp.addToPictureSizePreferances_elite2("picturestate_elite2", R.id.radioButton9);
                                myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(8).toString());
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            }
                            mPictureSizeValue.setTextColor(Color.WHITE);
                            break;
                    }
                    myapp.mCamera.setParameters(mCameraParametrs);
                }
            });
        } else {

            for (int i = 0; i < pixelArrayList.size(); i++) {
                radiobutton.get(i).setVisibility(View.VISIBLE);
                mViewsList.get(i).setVisibility(View.VISIBLE);
                radiobutton.get(i).setText(pixelArrayList.get(i));
                radiobutton.get(i).setTextSize(14);
                radiobutton.get(i).setCompoundDrawablesWithIntrinsicBounds(0, 0, android.support.v7.appcompat.R.drawable.abc_btn_radio_material, 0);
                radiobutton.get(i).setPadding(0, 7, 0, 7);
            }
            mSettingRadioGroup.check(myapp.PictureSizeStatusPreference.getInt("picturestate", 0));
            mSettingRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, int i) {
                    switch (radioGroup.getCheckedRadioButtonId()) {

                        case R.id.radioButton1:
                            mCameraParametrs.setPictureSize(mList.get(100).width, mList.get(100).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton1);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(0).toString());
                            Log.e("elite2:", "radioButtonQVGA:");
                            mPictureSizeValue.setText(pixelArrayList.get(0).toString());

                            break;
                        case R.id.radioButton2:
                            mCameraParametrs.setPictureSize(mList.get(200).width, mList.get(200).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton2);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(1).toString());
                            Log.e("elite2:", "radioButtonVGA:");
                            mPictureSizeValue.setText(pixelArrayList.get(1).toString());
                            break;
                        case R.id.radioButton3:
                            mCameraParametrs.setPictureSize(mList.get(1).width, mList.get(1).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton3);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(2).toString());
                            Log.e("elite2:", "radioButtonOnemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(2).toString());
                            break;
                        case R.id.radioButton4:
                            mCameraParametrs.setPictureSize(mList.get(2).width, mList.get(2).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton4);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(3).toString());
                            Log.e("elite2:", "radioButtonTwoMega:");
                            mPictureSizeValue.setText(pixelArrayList.get(3).toString());

                            break;
                        case R.id.radioButton5:
                            mCameraParametrs.setPictureSize(mList.get(3).width, mList.get(3).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton5);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(4).toString());
                            Log.e("elite2:", "radioButtonThreemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(4).toString());
                            break;
                        case R.id.radioButton6:
                            mCameraParametrs.setPictureSize(mList.get(4).width, mList.get(4).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton6);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(5).toString());
                            Log.e("elite2:", "radioButtonFourmega:");
                            mPictureSizeValue.setText(pixelArrayList.get(5).toString());
                            break;
                        case R.id.radioButton7:
                            mCameraParametrs.setPictureSize(mList.get(5).width, mList.get(5).height);
                            myapp.addToPictureSizePreferances("picturestate", R.id.radioButton7);
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(6).toString());
                            Log.e("elite2:btn7", "radioButtonFivemega:");
                            mPictureSizeValue.setText(pixelArrayList.get(6).toString());
                            break;
                        case R.id.radioButton8:
                            if (mList.get(6) != null) {
                                mCameraParametrs.setPictureSize(mList.get(6).width, mList.get(6).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton8);
                                Log.e("elite2:btn8=6", "radioButtonSixmega:");
                                myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(7).toString());

                            } else if (mList.get(7) != null) {
                                mCameraParametrs.setPictureSize(mList.get(7).width, mList.get(7).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton8);
                                myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(7).toString());
                                Log.e("elite2:btn8=7", "radioButtonSixmega:");

                            } else if (mList.get(8) != null) {
                                mCameraParametrs.setPictureSize(mList.get(8).width, mList.get(8).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton8);
                                myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(7).toString());
                                Log.e("elite2:btn8=8", "radioButtonSixmega:");

                            }
                            mPictureSizeValue.setText(pixelArrayList.get(7).toString());
                            break;
                        case R.id.radioButton9:
                            if (mList.get(10) != null) {
                                mCameraParametrs.setPictureSize(mList.get(10).width, mList.get(10).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton9);
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            } else if (mList.get(12) != null) {
                                mCameraParametrs.setPictureSize(mList.get(12).width, mList.get(12).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton9);
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            }
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(8).toString());
                            mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            break;

                        case R.id.radioButton10:
                            if (mList.get(11) != null) {
                                mCameraParametrs.setPictureSize(mList.get(11).width, mList.get(11).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton10);
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            } else if (mList.get(12) != null) {
                                mCameraParametrs.setPictureSize(mList.get(12).width, mList.get(12).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton10);
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(8).toString());
                            }
                            mPictureSizeValue.setText(pixelArrayList.get(9).toString());
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(9).toString());
                            break;
                        case R.id.radioButton11:
                            if (mList.get(13) != null) {
                                mCameraParametrs.setPictureSize(mList.get(13).width, mList.get(13).height);
                                myapp.addToPictureSizePreferances("picturestate", R.id.radioButton11);
                                Log.e("elite2:", "radioButtonSevanmega:");
                                mPictureSizeValue.setText(pixelArrayList.get(9).toString());
                            }
                            mPictureSizeValue.setText(pixelArrayList.get(10).toString());
                            myapp.addToStringValuePreferances("PICTURESIZE_STR", pixelArrayList.get(10).toString());
                            break;

                    }
                    mPictureSizeValue.setTextColor(Color.WHITE);
                    myapp.mCamera.setParameters(mCameraParametrs);
                }
            });
        }
    }
    //
}
