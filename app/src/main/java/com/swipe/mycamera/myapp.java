package com.swipe.mycamera;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;
import android.media.MediaPlayer;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Sangharatna on 10/29/2015.
 */
@SuppressWarnings("ALL")
public class myapp extends Application {

    public static Camera mCamera;
    public static SharedPreferences sharedPreferences;

    public static MediaPlayer mp,mp1,mp2,mp3,mp4,mp5;

    public static boolean QR=false;

    public static String Locality=null;

    public static int soundID = 0;
    public static String Progress = null;

    public static File defaultPath,internal,external;
    public static boolean isMemoryCardPresent=false;

    public static int counterframe =0;

    public static Context appContext;

    public static ImageView frame;

    public static String Gender = null;
    public static String Age = null;
    public static String Race = null;

    public static final int	BITRATE_LQ_240P		= 200000;
    public static final int	BITRATE_MQ_240P		= 500000;
    public static final int	BITRATE_HQ_240P		= 800000;
    public static final int	HEIGHT_240P			= 240;
    public static final int	WIDTH_240P			= 320;

    public static final int	BITRATE_LQ_360P		= 300000;
    public static final int	BITRATE_MQ_360P		= 700000;
    public static final int	BITRATE_HQ_360P		= 1000000;
    public static final int	HEIGHT_360P			= 360;
    public static final int	WIDTH_360P			= 640;

    public static final int	BITRATE_LQ_480P		= 750000;
    public static final int	BITRATE_MQ_480P		= 1750000;
    public static final int	BITRATE_HQ_480P		= 2500000;
    public static final int	HEIGHT_480P			= 480;
    public static final int	WIDTH_480P			= 640;

    public static final int	BITRATE_LQ_720P		= 1500000;
    public static final int	BITRATE_MQ_720P		= 3500000;
    public static final int	BITRATE_HQ_720P		= 5000000;
    public static final int	HEIGHT_720P			= 720;
    public static final int	WIDTH_720P			= 1280;

    public static final int	BITRATE_LQ_1080P	= 2400000;
    public static final int	BITRATE_MQ_1080P	= 5600000;
    public static final int	BITRATE_HQ_1080P	= 16000000;
    public static final int	HEIGHT_1080P		= 1080;
    public static final int	WIDTH_1080P			= 1920;




    public static final int	BITRATE_LQ_1440P	= 3000000;
    public static final int	BITRATE_MQ_1440P	= 7000000;
    public static final int	BITRATE_HQ_1440P	= 10000000;
    public static final int	HEIGHT_1440P		= 1440;
    public static final int	WIDTH_1440P			= 2560;

    public static final int	BITRATE_LQ_2160P	= 12000000;
    public static final int	BITRATE_MQ_2160P	= 28000000;
    public static final int	BITRATE_HQ_2160P	= 40000000;
    public static final int	HEIGHT_2160P		= 2160;
    public static final int	WIDTH_2160P			= 3840;

    public static boolean isHappy=false;

    public static String race =null;

    public static boolean isRecording=false;


    public static int	HEIGHT = 1080;
    public static int	WIDTH  = 1920;
    public static int	BITRATE  = 16000000;

    public static boolean isNoiseAllow;
    public static boolean isMicrophoneAllow=false;

    public static boolean isPauseRecording = false;

    public static ArrayList<String> fileSegments;


    // ApplicationAccess

    public static TextView timer;
    public static int mTimerVariable=0;
    public static TextView res360,res480,res720,res1080;
    public static TextView HDRTV;
    public static int zoom=0;
    public static int zoomVariable=0;
    public  static double mWidth;
    public  static double mHeight;
    public static boolean mCountDownTimer=false;
    public  static RelativeLayout GridLayout;
    public static int visibilityVariable=0;

    public static float prezoom=0;
    public static float preScale=0;
    public static SharedPreferences  LocationPreferences,ChackedStatusPreference,PictureSizeStatusPreference,
            PictureSizeStatusPreference_elite2,sharedPreferences_Exposure,sharedPreferences_Iso,sharedPreferences_picQuality,
            sharedPreferences_SeneMode,sharedPreferences_ColorEffect,sharedPreferences_AntiFlicker,sharedPreferences_SelfTimer,
            sharedPreferences_StringValue;



    //


    @Override
    public void onCreate() {
        super.onCreate();

        sharedPreferences = getSharedPreferences("CameraApp", MODE_PRIVATE);
        LocationPreferences = sharedPreferences;
        ChackedStatusPreference=sharedPreferences;
        PictureSizeStatusPreference=sharedPreferences;
        PictureSizeStatusPreference_elite2=sharedPreferences;
        sharedPreferences_Exposure=sharedPreferences;
        sharedPreferences_Iso=sharedPreferences;
        sharedPreferences_picQuality=sharedPreferences;
        sharedPreferences_ColorEffect=sharedPreferences;
        sharedPreferences_SeneMode=sharedPreferences;
        sharedPreferences_AntiFlicker=sharedPreferences;
        sharedPreferences_SelfTimer=sharedPreferences;
        sharedPreferences_StringValue=sharedPreferences;

        fileSegments = new ArrayList<String>();

        initStorage();
        initVideoResoulation();
        initVideoQuality();
        initNoise();
        initMicrophone();
        initBabySound();
        initQR();

        
    }

    private void initQR() {
        if(myapp.getStringFromSharedPreferances("QR")!=null)
        {
            if(myapp.getStringFromSharedPreferances("QR").equals("ON"))
            {
                myapp.QR=true;
            }
        }
        else
            myapp.QR=false;
    }
    private void initBabySound() {
        if(myapp.getStringFromSharedPreferances("SOUNDID")!=null)
        {
            soundID = Integer.parseInt(getStringFromSharedPreferances("SOUNDID"));
        }
    }

    private void initMicrophone() {
        //code to handle microphone
        if(myapp.getStringFromSharedPreferances("MICROPHONE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("MICROPHONE").equalsIgnoreCase("ON"))
            {
                myapp.isMicrophoneAllow=true;
            }
            else {
                myapp.isMicrophoneAllow=false;
            }
        }
    }

    private void initNoise() {
        if(myapp.getStringFromSharedPreferances("NOISE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("NOISE").equalsIgnoreCase("ALLOW"))
                myapp.isNoiseAllow=true;
            else if(myapp.getStringFromSharedPreferances("NOISE").equalsIgnoreCase("NOTALLOW"))
            {
                myapp.isNoiseAllow=false;
            }
        }
        else
            myapp.isNoiseAllow=true;
    }

    private void initVideoQuality() {
        if(myapp.getStringFromSharedPreferances("QUALITY")!=null) {

            if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("Low")) {
                myapp.BITRATE = 2500000;
            } else if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("Medium")) {
                myapp.BITRATE = 5000000;
            } else if (myapp.getStringFromSharedPreferances("QUALITY").equalsIgnoreCase("High")) {
                myapp.BITRATE = 8000000;
            }
        }
    }

    private void initVideoResoulation() {

        if(myapp.getStringFromSharedPreferances("RESOLUTION")!=null) {

            if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("360P")) {
                myapp.HEIGHT = myapp.HEIGHT_240P;
                myapp.WIDTH = myapp.WIDTH_240P;
            } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("480P")) {
                myapp.HEIGHT = myapp.HEIGHT_480P;
                myapp.WIDTH = myapp.WIDTH_480P;
            } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("720P")) {
                myapp.HEIGHT = myapp.HEIGHT_720P;
                myapp.WIDTH = myapp.WIDTH_720P;
            } else if (myapp.getStringFromSharedPreferances("RESOLUTION").equalsIgnoreCase("1080P")) {
                myapp.HEIGHT = myapp.HEIGHT_1080P;
                myapp.WIDTH = myapp.WIDTH_1080P;
            }
        }
    }

    public static void initStorage() {

        defaultPath = new File("/mnt/sdcard/SwipeCamera");
        if(!defaultPath.exists())
            defaultPath.mkdir();

        internal = new File("/mnt/sdcard2"); //internal path
        external = new File("/mnt/sdcard"); // ExternaCard Path

        if(internal.canWrite() && external.canWrite())
            isMemoryCardPresent=true;
        else
            isMemoryCardPresent=false;

        if(isMemoryCardPresent)
        {
            if(getStringFromSharedPreferances("STORAGE")!=null)
            {
                if(getStringFromSharedPreferances("STORAGE").equalsIgnoreCase("PHONE"))
                {
                    File d = new File("/mnt/sdcard2/SwipeCamera");
                    if(!d.exists())
                        d.mkdir();
                    defaultPath=d;
                }
                else if(getStringFromSharedPreferances("STORAGE").equalsIgnoreCase("SDCARD"))
                {
                    File d = new File("/mnt/sdcard/SwipeCamera");
                    if(!d.exists())
                        d.mkdir();
                    defaultPath=d;
                }
            }
        }
    }

    public static void addToSharedPreferances(String key,String value)
    {
        SharedPreferences.Editor et = sharedPreferences.edit();
        et.putString(key, value);
        et.apply();
    }
    public static void removeFromSharedPreferances(String key)
    {
        SharedPreferences.Editor et = sharedPreferences.edit();
        et.remove(key);
        et.apply();
    }
    public static String getStringFromSharedPreferances(String key)
    {
        String Value=null;
        //if(sharedPreferences.getString(key,"")!=null)
        if(sharedPreferences.contains(key))
        {
            Value = sharedPreferences.getString(key,"");
        }
        return Value;
    }
    public static String getStringFromSharedPreferencesValues(String key)
    {
        String Value=null;
        //if(sharedPreferences.getString(key,"")!=null)
        if(sharedPreferences_StringValue.contains(key))
        {
            Value = sharedPreferences_StringValue.getString(key,"");
        }
        return Value;
    }

    //appaccess
    public static void addToSelfTimerPreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_SelfTimer.edit();
        et.putInt(key, value);
        et.apply();
    }

    public static void addToHdrModePreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_AntiFlicker.edit();
        et.putInt(key, value);
        et.apply();
    }

    public static void addToFlickerModePreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_AntiFlicker.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToSceneModePreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_SeneMode.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToColorPreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_ColorEffect.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToPicQualityPreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_picQuality.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToExposurePreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_Exposure.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToIsoPreferences(String key,int value)
    {
        SharedPreferences.Editor et = sharedPreferences_Iso.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToPictureSizePreferances_elite2(String key,int value)
    {
        SharedPreferences.Editor et = PictureSizeStatusPreference_elite2.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToPictureSizePreferances(String key,int value)
    {
        SharedPreferences.Editor et = PictureSizeStatusPreference.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToCheckedPreferances(String key,int value)
    {
        SharedPreferences.Editor et = ChackedStatusPreference.edit();
        et.putInt(key, value);
        et.apply();
    }
    public static void addToLocationPreferances(String key,double value)
    {
        SharedPreferences.Editor et = LocationPreferences.edit();
        et.putLong(key, Double.doubleToLongBits(value));
        et.apply();
    }
    public static void addToStringValuePreferances(String key,String value)
    {
        SharedPreferences.Editor et = sharedPreferences_StringValue.edit();
        et.putString(key, value);
        et.apply();
    }

    public static void removeFromLocationPreferances(String key)
    {
        SharedPreferences.Editor et = LocationPreferences.edit();
        et.remove(key);
        et.apply();
    }

    public static double getMegapixels(double width,double height)
    {
        double result=0;

        result=width*height/1000000;
        return result;
    }
    public static void setSelfTimerInput(int var){
        mTimerVariable=var;
    }
    public static int getSelfTimerInput(){
        return mTimerVariable;
    }
    //
}
