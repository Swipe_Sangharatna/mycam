package com.swipe.mycamera;

import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.vision.MultiProcessor;
import com.google.android.gms.vision.Tracker;
import com.google.android.gms.vision.face.Face;
import com.google.android.gms.vision.face.FaceDetector;
import com.googlecode.mp4parser.BasicContainer;
import com.googlecode.mp4parser.authoring.Movie;
import com.googlecode.mp4parser.authoring.Track;
import com.googlecode.mp4parser.authoring.builder.DefaultMp4Builder;
import com.googlecode.mp4parser.authoring.container.mp4.MovieCreator;
import com.googlecode.mp4parser.authoring.tracks.AppendTrack;
import com.swipe.mycamera.camera.CameraSource;
import com.swipe.mycamera.camera.CameraSourcePreview;
import com.swipe.mycamera.camera.GraphicOverlay;
import com.swipe.mycamera.slidebutton.BabyCam;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class MainView extends AppCompatActivity implements SurfaceHolder.Callback,Camera.PreviewCallback{

    //faceTracker

    int delay = 1000; // delay for 1 sec.
    int period = 2005; // repeat every 10 sec.
    static Timer timer;

    static boolean isRecording = false;

    private CameraSource mCameraSource = null;

    public CameraSourcePreview mPreview;
    private GraphicOverlay mGraphicOverlay;
    public RelativeLayout imageviewcap;

    private static final int RC_HANDLE_GMS = 9001;
    // permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    float mDist;

    ImageView sceneMode;
    AlertDialog dialogScene;

    TextView sceneModeTitle;

    //
    //swipe cam merge

    SurfaceView surfaceView;
    SurfaceHolder surfaceHolder;
    //Camera myapp.mCamera;
    Boolean checked=false;
    static MediaRecorder mediaRecorder;
    private static  final int FOCUS_AREA_SIZE= 300;

    MediaPlayer babyplayer;

    Button start;
    Button stop;
    Button capture;

    ImageView settingView,thumb,switchcam,switchbaby,panorama;
    Button pausebtn;

    MyPTsansTextView zoomTV;

    String VIDEO_FILENAME;
    boolean isFrontCam= false;
    String IMG_FILENAME;

    RelativeLayout timerlay;
    TextView timetv;

    int angle=90;

    ImageView flash,demoView;

    int hr=0;
    int mm=0;
    int y=0;
    int sec=0;
    Timer myTimer;

    int flag=0;

    //

    CirclePageIndicator indicator;
    ViewPager viewPager;

    View v,v1,v2;

    ArrayList<View> layouts;


    ImageView baby;

    ScaleGestureDetector scaleGestureDetector;

    RelativeLayout none,mono,sepia,negative,aqua,whiteborad,blackboard,candle,posterize;
    GPSTracker gpsTracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main_view);

        myapp.appContext = this;


        layouts =new ArrayList<View>();

        v = getLayoutInflater().inflate(R.layout.settings,null);
        v1 = getLayoutInflater().inflate(R.layout.camview,null);
        v2 = getLayoutInflater().inflate(R.layout.scene,null);

        initViews();



        //nt x = 15/0;

        layouts.add(v);
        layouts.add(v1);
        layouts.add(v2);

        Adapter adapter = new Adapter();
        viewPager.setAdapter(adapter);

        viewPager.setCurrentItem(1);

        indicator.setViewPager(viewPager);
        initFace();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                initCam();

                scaleGestureDetector = new ScaleGestureDetector(MainView.this, new simpleOnScaleGestureListener());

                initFlashMode();

                gpsTracker = new GPSTracker(MainView.this);
                new Location().execute();

            }
        }, 1000);

        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if (myapp.isHappy) {
                    if (myapp.getStringFromSharedPreferances("SMILE") != null) {
                        if (myapp.getStringFromSharedPreferances("SMILE").equals("ON")) {
                            CaptureImage();
                            myapp.isHappy = false;
                        }
                    }
                }
            }
        }, delay, period);

        baby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(MainView.this, BabyCam.class);
                startActivity(in);
                overridePendingTransition(R.anim.slideinleft, R.anim.slideinright);
            }
        });
        initFilter();
        initScenes();

    }

    private void initScenes() {



        sceneMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder ab = new AlertDialog.Builder(MainView.this, R.style.AppCompatAlertDialogStyle);
                ab.setTitle("Scene Modes");
                View sceneView = getLayoutInflater().inflate(R.layout.scenemodes, null);

                TextView autotv = (TextView) sceneView.findViewById(R.id.textView3);
                TextView nighttv = (TextView) sceneView.findViewById(R.id.textView36);
                TextView sunsettv = (TextView) sceneView.findViewById(R.id.textView35);
                TextView partytv = (TextView) sceneView.findViewById(R.id.textView32);
                TextView potraittv = (TextView) sceneView.findViewById(R.id.textView34);
                TextView landscapetv = (TextView) sceneView.findViewById(R.id.textView28);
                TextView theatertv = (TextView) sceneView.findViewById(R.id.textView33);
                TextView sporttv = (TextView) sceneView.findViewById(R.id.textView30);
                TextView fireworktv = (TextView) sceneView.findViewById(R.id.textView29);
                TextView snowtv = (TextView) sceneView.findViewById(R.id.textView31);

                autotv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        sceneModeTitle.setText("Auto");

                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.automode));
                        dialogScene.dismiss();

                    }
                });
                nighttv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);

                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_NIGHT);
                        myapp.mCamera.setParameters(param1);

                        sceneModeTitle.setText("Night");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.nightmode));
                        dialogScene.dismiss();

                    }
                });
                sunsettv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();

                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_SUNSET);
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Sunset");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.sunsetmode));
                        dialogScene.dismiss();

                    }
                });
                partytv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_PARTY);
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.partymode));
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Party");
                        dialogScene.dismiss();

                    }
                });
                potraittv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_PORTRAIT);
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Potrait");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.portratmode));
                        dialogScene.dismiss();

                    }
                });
                landscapetv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_LANDSCAPE);
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Landscape");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.landscapemode));
                        dialogScene.dismiss();

                    }
                });
                theatertv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_THEATRE);
                        myapp.mCamera.setParameters(param1);
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.thetermode));
                        dialogScene.dismiss();

                    }
                });
                sporttv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);
                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_SPORTS);
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Sport");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.sportmode));
                        dialogScene.dismiss();

                    }
                });
                fireworktv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);

                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_FIREWORKS);
                        myapp.mCamera.setParameters(param1);
                        sceneModeTitle.setText("Firework");
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.fireworkmode));
                        dialogScene.dismiss();

                    }
                });
                snowtv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                        myapp.mCamera.setParameters(param);

                        Camera.Parameters param1 = myapp.mCamera.getParameters();
                        param1.setSceneMode(Camera.Parameters.SCENE_MODE_SNOW);
                        myapp.mCamera.setParameters(param1);
                        sceneMode.setImageDrawable(getResources().getDrawable(R.mipmap.snowmode));
                        sceneModeTitle.setText("Snow");
                        dialogScene.dismiss();

                    }
                });
                ab.setView(sceneView);
                ab.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialogScene.dismiss();
                    }
                });
                dialogScene = ab.create();
                dialogScene.show();
                dialogScene.getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT,750);
            }
        });
    }

    private void initFilter() {

        none.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                parm.setColorEffect(Camera.Parameters.EFFECT_NONE);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        mono.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_MONO);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        sepia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_SEPIA);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        negative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_NEGATIVE);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        aqua.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_AQUA);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        blackboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_BLACKBOARD);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        whiteborad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_WHITEBOARD);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        candle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_NONE);
                parm.setSceneMode(Camera.Parameters.SCENE_MODE_CANDLELIGHT);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
        posterize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Camera.Parameters parm = myapp.mCamera.getParameters();
                parm.setColorEffect(Camera.Parameters.EFFECT_POSTERIZE);
                myapp.mCamera.setParameters(parm);
                viewPager.setCurrentItem(1);
            }
        });
    }
    private void initFlashMode() {
        Camera.Parameters p = myapp.mCamera.getParameters();
        if(myapp.getStringFromSharedPreferances("FLASHMODE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("ON"))
            {
                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flash));
            }
            else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("OFF"))
            {
                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashoff));
            }
            else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("AUTO"))
            {
                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashauto));
            }
        }
    }

    private void initFace() {
        mPreview = (CameraSourcePreview) v1.findViewById(R.id.preview);
        mGraphicOverlay = (GraphicOverlay) v1.findViewById(R.id.faceOverlay);
        createCameraSource();

    }
    private void createCameraSource() {
        Context context;
        context = getApplicationContext();
        FaceDetector detector=null;
        if(myapp.getStringFromSharedPreferances("FACE")!=null)
        {
            if(myapp.getStringFromSharedPreferances("FACE").equals("ON"))
            {
                detector = new FaceDetector.Builder(context)
                        .setClassificationType(FaceDetector.ALL_CLASSIFICATIONS)
                        .build();
                detector.setProcessor(
                        new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                                .build());
                if (!detector.isOperational()) {
                    Log.w("TAG", "Face detector dependencies are not yet available.");
                }
            }
            else
            {
                detector = new FaceDetector.Builder(context)
                        .setClassificationType(FaceDetector.FAST_MODE)
                        .build();

                /*detector.setProcessor(
                        new MultiProcessor.Builder<>(new GraphicFaceTrackerFactory())
                                .build());*/
            }
        }
        if(myapp.getStringFromSharedPreferances("CAMERAFLAG")!=null)
        {
            if(myapp.getStringFromSharedPreferances("CAMERAFLAG").equalsIgnoreCase("1"))
            {
                flag=1;
                    mCameraSource = new CameraSource.Builder(context, detector)
                            .setRequestedPreviewSize(1920, 1080)
                            .setFacing(CameraSource.CAMERA_FACING_FRONT)
                            .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
                            .setRequestedFps(30.0f)
                            .build();
            }
            else {
                flag = 0;
                mCameraSource = new CameraSource.Builder(context, detector)
                        .setRequestedPreviewSize(1920, 1080)  //3840x2160
                        .setFacing(CameraSource.CAMERA_FACING_BACK)
                        .setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO)
                        .setRequestedFps(30.0f)
                        .build();
            }
        }
        else
        {
            mCameraSource = new CameraSource.Builder(context, detector)
                    .setRequestedPreviewSize(1920,1080)  //3840x2160
                    .setFacing(CameraSource.CAMERA_FACING_BACK)
                    .setRequestedFps(30.0f)
                    .build();
        }
        startCamSource();
    }
    public void startCamSource()
    {
        try {
            mPreview.start(mCameraSource, mGraphicOverlay);
        } catch (IOException e) {
            Log.e("StartPreview","Exception");
            e.printStackTrace();
        }
    }
    private class GraphicFaceTrackerFactory implements MultiProcessor.Factory<Face> {
        @Override
        public Tracker<Face> create(Face face) {
            return new GraphicFaceTracker(mGraphicOverlay);
        }
    }
    private class GraphicFaceTracker extends Tracker<Face> {

        private GraphicOverlay mOverlay;
        private FaceGraphic mFaceGraphic;

        GraphicFaceTracker(GraphicOverlay overlay) {
            mOverlay = overlay;
            mFaceGraphic = new FaceGraphic(overlay);
        }
        /**
         * Start tracking the detected face instance within the face overlay.
         */
        @Override
        public void onNewItem(int faceId, Face item) {
            mFaceGraphic.setId(faceId);
        }
        /**
         * Update the position/characteristics of the face within the overlay.
         */
        @Override
        public void onUpdate(FaceDetector.Detections<Face> detectionResults, Face face) {
            mOverlay.add(mFaceGraphic);
            mFaceGraphic.updateFace(face);
        }

        /**
         * Hide the graphic when the corresponding face was not detected.  This can happen for
         * intermediate frames temporarily (e.g., if the face was momentarily blocked from
         * view).
         */
        @Override
        public void onMissing(FaceDetector.Detections<Face> detectionResults) {
            mOverlay.remove(mFaceGraphic);
        }

        /**
         * Called when the face is assumed to be gone for good. Remove the graphic annotation from
         * the overlay.
         */
        @Override
        public void onDone() {
            mOverlay.remove(mFaceGraphic);
        }
    }

    private void initCam() {

        if(myapp.getStringFromSharedPreferances("SOUNDID")!=null)
        {
            if(myapp.getStringFromSharedPreferances("BABY")!=null)
                if(myapp.getStringFromSharedPreferances("BABY").equals("ON"))
                    switchbaby.setVisibility(View.VISIBLE);
                else
                    switchbaby.setVisibility(View.INVISIBLE);
        }
        else
            switchbaby.setVisibility(View.INVISIBLE);

        switchbaby.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (babyplayer != null) {
                    babyplayer.release();
                    babyplayer = null;
                }
                babyplayer = MediaPlayer.create(MainView.this, myapp.soundID);
                babyplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        babyplayer.release();
                        babyplayer = null;
                    }
                });
                babyplayer.start();
            }
        });

        if(myapp.mCamera!=null) {

            myapp.fileSegments.clear();
            Camera.Parameters para = myapp.mCamera.getParameters();
            para.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_AUTO);
            para.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
            para.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
            para.setAntibanding(Camera.Parameters.ANTIBANDING_AUTO);
            para.setPreviewSize(myapp.WIDTH, myapp.HEIGHT);
            para.setPictureSize(myapp.WIDTH, myapp.HEIGHT);
            para.setVideoStabilization(true);
            myapp.mCamera.setParameters(para);
        }
        Log.e("camera", "Size parameters: " + myapp.WIDTH + " >>" + myapp.HEIGHT);

        switchcam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (flag == 0) {

                    if (myapp.mCamera.getNumberOfCameras() > 1) {

                        flag = 1;
                        angle = 270;
                        myapp.addToSharedPreferances("CAMERAFLAG", "1");

                        ObjectAnimator animation = ObjectAnimator.ofFloat(mPreview, "rotationY", 0.0f, 360f);
                        animation.setDuration(3600);
                        animation.setInterpolator(new AccelerateDecelerateInterpolator());
                        animation.start();

                        restartCam(angle);
                    }
                } else {
                    angle = 90;
                    flag = 0;
                    myapp.addToSharedPreferances("CAMERAFLAG", "0");


                    ObjectAnimator animation = ObjectAnimator.ofFloat(mPreview, "rotationY", 0.0f, 360f);
                    animation.setDuration(3600);
                    animation.setInterpolator(new AccelerateDecelerateInterpolator());
                    animation.start();

                    restartCam(angle);
                }
            }
        });
        settingView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //settingView.setVisibility(View.INVISIBLE);
                //int x = 6/0;
                Intent in = new Intent(MainView.this, Settings.class);
                startActivity(in);
                overridePendingTransition(android.R.anim.slide_in_left, android.R.anim.slide_out_right);
            }
        });
        flash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {

                    final Camera.Parameters param = myapp.mCamera.getParameters();

                    if (myapp.isRecording) {
                        //code for camcorder
                        Log.e("CAMERA", "RECORDING");
                        if (param.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)) {
                            try {
                                param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                                myapp.mCamera.setParameters(param);
                                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flash));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            try {
                                param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                                myapp.mCamera.setParameters(param);
                                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashoff));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    } else {
                        Log.e("CAMERA", "CAPTURE " + param.getFlashMode());
                        //code for capture view
                        if (param.getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF)) {
                            param.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                            myapp.mCamera.setParameters(param);

                            myapp.addToSharedPreferances("FLASHMODE", "AUTO");

                            flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashauto));
                        } else if (param.getFlashMode().equals(Camera.Parameters.FLASH_MODE_AUTO)) {
                            myapp.addToSharedPreferances("FLASHMODE", "ON");
                            param.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                            myapp.mCamera.setParameters(param);

                            flash.setImageDrawable(getResources().getDrawable(R.mipmap.flash));
                        } else if (param.getFlashMode().equals(Camera.Parameters.FLASH_MODE_ON)) {
                            myapp.addToSharedPreferances("FLASHMODE", "OFF");
                            param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                            myapp.mCamera.setParameters(param);
                            flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashoff));
                        }
                    }
                }
            }
        });

        initTouchToFocus(mPreview, mGraphicOverlay);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CaptureImage();
            }
        });

        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashoff));
                Camera.Parameters p = myapp.mCamera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                myapp.mCamera.setParameters(p);
                myapp.addToSharedPreferances("FLASHMODE","OFF");

                hideUI();
                initstart();
                myapp.isRecording=true;
            }
        });
        stop.setClickable(false);
        stop.setEnabled(false);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initStop();
                showUI();
                flash.setImageDrawable(getResources().getDrawable(R.mipmap.flashoff));
                Camera.Parameters p = myapp.mCamera.getParameters();
                p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                myapp.mCamera.setParameters(p);
                myapp.addToSharedPreferances("FLASHMODE", "OFF");
                myapp.isRecording=false;
            }
        });

        if(myapp.getStringFromSharedPreferances("VIDEOFILE")!=null)
        {
            try {

                if(myapp.getStringFromSharedPreferances("VIDEOFILETYPE").equals("VIDEO")) {
                    thumb.setImageBitmap(retriveVideoFrameFromVideo(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                    thumb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                            intent.setDataAndType(Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")), "video/mp4");
                            //finishAffinity();
                            startActivity(intent);
                        }
                    });
                }
                else if(myapp.getStringFromSharedPreferances("VIDEOFILETYPE").equals("IMG"))
                {
                    Bitmap b = BitmapFactory.decodeFile(myapp.getStringFromSharedPreferances("VIDEOFILE"));
                    b = Bitmap.createScaledBitmap(b, 120, 120, false);
                    thumb.setImageBitmap(b);

                    thumb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                            intent.setDataAndType(Uri.parse("file://"+myapp.getStringFromSharedPreferances("VIDEOFILE")), "image/*");
                            startActivity(intent);
                        }
                    });
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
        else
        {
            thumb.setVisibility(View.INVISIBLE);
            //thumb2.setVisibility(View.INVISIBLE);
        }
    }
    private void initViews() {

        imageviewcap = (RelativeLayout) v1.findViewById(R.id.imgviewlayout);

        sceneModeTitle = (TextView) v.findViewById(R.id.textViewTitle);

        sceneMode = (ImageView) v.findViewById(R.id.imageViewIcon);

        baby = (ImageView) v.findViewById(R.id.imageViewIcon5);

        panorama = (ImageView) v1.findViewById(R.id.imageView10);

        switchbaby = (ImageView) v1.findViewById(R.id.switchbaby);

        zoomTV = (MyPTsansTextView) v1.findViewById(R.id.textView17);

        viewPager = (ViewPager) findViewById(R.id.pager);
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        //surfaceView = (SurfaceView) v1.findViewById(R.id.surfaceView);
        start = (Button) v1.findViewById(R.id.button);
        stop = (Button) v1.findViewById(R.id.button2);
        capture = (Button) v1.findViewById(R.id.button3);
        myapp.frame = (ImageView) v1.findViewById(R.id.imageView5);
        settingView = (ImageView) v1.findViewById(R.id.settingiv);
        flash = (ImageView) v1.findViewById(R.id.imageView4);
        thumb = (ImageView) v1.findViewById(R.id.thumb);
        //thumb2 = (ImageView) v1.findViewById(R.id.thumb2);

        timetv = (TextView) v1.findViewById(R.id.timertv);
        timerlay = (RelativeLayout) v1.findViewById(R.id.timelayout);
        switchcam = (ImageView) v1.findViewById(R.id.switchcamiv);

        demoView = (ImageView) v1.findViewById(R.id.imageView6);

        pausebtn = (Button) v1.findViewById(R.id.button343);
        none = (RelativeLayout) v2.findViewById(R.id.noneeffectrl);
        mono = (RelativeLayout) v2.findViewById(R.id.monoeffectrl);
        sepia = (RelativeLayout) v2.findViewById(R.id.sepiaeffectrl);
        negative = (RelativeLayout) v2.findViewById(R.id.negativeeffectrl);
        aqua = (RelativeLayout) v2.findViewById(R.id.aquaeffectrl);
        whiteborad = (RelativeLayout) v2.findViewById(R.id.whiteboardeffectrl);
        blackboard = (RelativeLayout) v2.findViewById(R.id.blackboardeffectrl);
        candle = (RelativeLayout) v2.findViewById(R.id.candleeffectrl);
        posterize = (RelativeLayout) v2.findViewById(R.id.postereffectrl);
    }

    public class Adapter extends PagerAdapter{

        @Override
        public int getCount() {
            return layouts.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == ((View) object);
        }
        @Override
        public Object instantiateItem(ViewGroup container, int position) {

            View v = layouts.get(position);

            ((ViewPager) container).addView(layouts.get(position), 0);

            Log.e("Position","??"+position);
            return v;
        }
        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            ((ViewPager) container).removeView((View) object);
        }
    }

    //Swipe <code></code>

    public void initRecordPause()
    {
        mediaRecorder.stop();
        myTimer.cancel();
        if(sec!=0)
            sec=sec-1;
        myapp.fileSegments.add((myapp.defaultPath + "/" + VIDEO_FILENAME));

    }
    private void initStop() {
        if(!myapp.isPauseRecording) {

            mediaRecorder.stop();

            myTimer.cancel();

            timerlay.setVisibility(View.INVISIBLE);

            pausebtn.setBackground(getDrawable(R.mipmap.pus));
            pausebtn.setVisibility(View.INVISIBLE);
            start.setClickable(true);
            start.setEnabled(true);
            stop.setClickable(false);
            stop.setEnabled(false);
            start.setVisibility(View.VISIBLE);
            stop.setVisibility(View.INVISIBLE);


            myapp.addToSharedPreferances("VIDEOFILE", (myapp.defaultPath + "/" + VIDEO_FILENAME));
            myapp.addToSharedPreferances("VIDEOFILETYPE","VIDEO");
            thumb.setVisibility(View.VISIBLE);
            //thumb2.setVisibility(View.VISIBLE);

            try {
                if(myapp.getStringFromSharedPreferances("VIDEOFILETYPE").equals("VIDEO")) {
                    thumb.setImageBitmap(retriveVideoFrameFromVideo(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                    thumb.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                            intent.setDataAndType(Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")), "video/mp4");
                            //finishAffinity();
                            startActivity(intent);

                        }
                    });
                    sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(myapp.defaultPath + "/" + VIDEO_FILENAME))));
                }
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }else
        {
            try{
                mediaRecorder.stop();
            }catch (Exception e){}

            myTimer.cancel();
            pausebtn.setVisibility(View.INVISIBLE);
            pausebtn.setBackground(getDrawable(R.mipmap.pus));
            start.setClickable(true);
            start.setEnabled(true);
            stop.setClickable(false);
            stop.setEnabled(false);
            start.setVisibility(View.VISIBLE);
            stop.setVisibility(View.INVISIBLE);
            timerlay.setVisibility(View.INVISIBLE);
            //call here merge task and after get thumbnails

            myapp.fileSegments.add((myapp.defaultPath + "/" + VIDEO_FILENAME));

            new MergeVide().execute();
            myapp.isPauseRecording=false;
        }
    }

    private void initstart() {

        initTimer();

        initPause();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());

        VIDEO_FILENAME = "VID_" + currentDateandTime + ".mp4";

        if(flag==1)
            initRecorder(myapp.isMicrophoneAllow,270);
        else
            initRecorder(myapp.isMicrophoneAllow, 90);

        myapp.mCamera.stopPreview();
        myapp.mCamera.unlock();

        start.setVisibility(View.INVISIBLE);
        stop.setVisibility(View.VISIBLE);

        try {
            mediaRecorder.prepare();
            mediaRecorder.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
        start.setClickable(false);
        start.setEnabled(false);
        stop.setClickable(true);
        stop.setEnabled(true);
    }

    private void initPause() {


        pausebtn.setVisibility(View.VISIBLE);
        pausebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (y == 0) {
                    myapp.isPauseRecording = true;
                    pausebtn.setBackground(getDrawable(R.mipmap.ply));
                    myTimer.cancel();
                    initRecordPause();
                    y = 1;
                } else {
                    y = 0;
                    try {
                        myTimer.cancel();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    initstart();
                    pausebtn.setBackground(getDrawable(R.mipmap.pus));
                }
            }
        });
    }

    private void initTimer() {

        if(!myapp.isPauseRecording) {
            hr = 0;
            mm = 0;
            sec = 0;
        }

        timerlay.setVisibility(View.VISIBLE);

        myTimer = new Timer();
        myTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                TimerMethod();
            }

        }, 0, 1000);

    }
    private void TimerMethod()
    {
        this.runOnUiThread(Timer_Tick);
    }
    private Runnable Timer_Tick = new Runnable() {

        public void run() {
            if(sec==60)
            {
                mm++;
                sec=0;
            }
            if(mm==60) {
                hr++;
                mm=0;
            }
            if(sec<10)
                timetv.setText("0"+hr+" : 0"+mm+" : 0"+sec);
            else
                timetv.setText("0"+hr+" : 0"+mm+" : "+sec);
            sec++;
        }
    };

    boolean startContinuousAutoFocus() {

        Camera.Parameters params = myapp.mCamera.getParameters();

        List<String> focusModes = params.getSupportedFocusModes();

        String CAF_PICTURE = Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE,
                CAF_VIDEO = Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO,
                supportedMode = focusModes
                        .contains(CAF_PICTURE) ? CAF_PICTURE : focusModes
                        .contains(CAF_VIDEO) ? CAF_VIDEO : "";

        if (!supportedMode.equals("")) {

            params.setFocusMode(supportedMode);
            myapp.mCamera.setParameters(params);
            return true;
        }

        return false;
    }



    private void initTouchToFocus(CameraSourcePreview surfaceView, GraphicOverlay mGraphicOverlay) {
        if(myapp.getStringFromSharedPreferances("FOCUS")!=null) {

            if(myapp.getStringFromSharedPreferances("FOCUS").equalsIgnoreCase("ON")) {
                Log.e("Touch","INITIATED");
                mGraphicOverlay.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        Log.e("Touch CLick", "True");

                        if(event.getPointerCount()>1) {
                            handleZoom(event, myapp.mCamera.getParameters());
                            scaleGestureDetector.onTouchEvent(event);
                        }
                        else
                        {
                            Log.e("SINGLE","POINTER");
                            focusOnTouch(event);
                        }
                       /* if (event.getAction() == MotionEvent.ACTION_DOWN) {
                            focusOnTouch(event);
                        }*/
                        return true;
                    }
                });
            }else
            {
                surfaceView.setOnTouchListener(null);
                surfaceView.setOnTouchListener(null);
            }
        }else
        {
            surfaceView.setOnTouchListener(null);
        }
    }

    private void CaptureImage() {

        if(myapp.mCamera!=null) {

            Log.e("isRecording"," >"+isRecording);

            if(myapp.isRecording)
            if(myapp.mCamera.getParameters().getFlashMode().equals(Camera.Parameters.FLASH_MODE_OFF))
            {
                Log.e("insideFlash","true");
                Camera.Parameters param = myapp.mCamera.getParameters();
                param.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                myapp.mCamera.setParameters(param);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Camera.Parameters param = myapp.mCamera.getParameters();
                        param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        myapp.mCamera.setParameters(param);
                    }
                },1000);
            }
            try {
                myapp.mCamera.takePicture(null, null, mPicture);
                // Rotait IMG code
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        String photopath = IMG_FILENAME;

                        File tempFile = new File(photopath);

                        Bitmap bmp = BitmapFactory.decodeFile(photopath);

                        Matrix matrix = new Matrix();
                        //matrix.postRotate(angle);
                        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

                        FileOutputStream fOut;
                        try {

                            fOut = new FileOutputStream(tempFile);
                            bmp.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                            fOut.flush();
                            fOut.close();

                            sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(new File(IMG_FILENAME))));


                        } catch (FileNotFoundException e1) {
                            // TODO Auto-generated catch block
                            e1.printStackTrace();
                        } catch (IOException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                    }
                }, 3500);
            } catch (Throwable t) {
                t.printStackTrace();
            }
        }
    }

    private void initRecorder(boolean isChecked,int rotation) {

        if (myapp.getStringFromSharedPreferances("HDR") != null) {
            if (myapp.getStringFromSharedPreferances("HDR").equalsIgnoreCase("ON")) {
                Camera.Parameters params = myapp.mCamera.getParameters();
                params.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
                myapp.mCamera.setParameters(params);
            } else {
                Camera.Parameters params = myapp.mCamera.getParameters();
                params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
                myapp.mCamera.setParameters(params);
            }
        }

        if(mediaRecorder!=null) {
            mediaRecorder.release();
            mediaRecorder = null;
        }

        if (!isChecked) {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setCamera(myapp.mCamera);
            //mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            //File f = new File("/mnt/sdcard/abc", "My.mp4");
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setOutputFile(myapp.defaultPath + "/" + VIDEO_FILENAME);
            //mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setVideoSize(myapp.WIDTH, myapp.HEIGHT);
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);

            //if(rotation!=0)
            mediaRecorder.setOrientationHint(rotation);
            Log.e("W H",">> "+myapp.WIDTH+" "+myapp.HEIGHT);

            mediaRecorder.setVideoEncodingBitRate(myapp.BITRATE);

//                mediaRecorder.setVideoFrameRate(30);

        } else {
            mediaRecorder = new MediaRecorder();
            mediaRecorder.setCamera(myapp.mCamera);
               /* if (myapp.isNoiseAllow)
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                else
                    mediaRecorder.setAudioSource(MediaRecorder.AudioSource.CAMCORDER);*/
            mediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
            mediaRecorder.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            //File f = new File("/mnt/sdcard/abc", "My.mp4");
            mediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
            mediaRecorder.setOutputFile(myapp.defaultPath + "/" + VIDEO_FILENAME);
            mediaRecorder.setVideoSize(myapp.WIDTH, myapp.HEIGHT);
            mediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
            mediaRecorder.setVideoEncoder(MediaRecorder.VideoEncoder.MPEG_4_SP);
//              mediaRecorder.setVideoFrameRate(30);
            //if(rotation!=0)
            Log.e("W H", ">> " + myapp.WIDTH + " " + myapp.HEIGHT);
            mediaRecorder.setOrientationHint(rotation);
            mediaRecorder.setVideoEncodingBitRate(myapp.BITRATE);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.e("INSIDE","STOP");
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            myapp.mCamera.setPreviewDisplay(surfaceHolder);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        Camera.Parameters params = myapp.mCamera.getParameters();
        //params.setPreviewSize(1920,1080);
        params.setPreviewSize(myapp.WIDTH, myapp.HEIGHT);
        params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        myapp.mCamera.setParameters(params);
        myapp.mCamera.startPreview();

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    Camera.PictureCallback mPicture = new Camera.PictureCallback() {
        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            //File pictureFile = new File("/mnt/sdcard/abc/mypic.png");

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String currentDateandTime = sdf.format(new Date());

            File pictureFile = new File(myapp.defaultPath.getPath()+"/IMG_"+currentDateandTime+".png");

            IMG_FILENAME = myapp.defaultPath.getPath()+"/IMG_"+currentDateandTime+".png";

            Log.e("IMG","?? "+IMG_FILENAME);

            if (pictureFile == null) {
                return;
            }
            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                fos.write(data);
                fos.close();
                Log.e("Image","Captured");
            } catch (FileNotFoundException e) {

            } catch (IOException e) {
            }
            myapp.mCamera.stopPreview();
            myapp.mCamera.startPreview();


            try {

                myapp.addToSharedPreferances("VIDEOFILE",IMG_FILENAME);
                myapp.addToSharedPreferances("VIDEOFILETYPE","IMG");
                Bitmap b = BitmapFactory.decodeFile(IMG_FILENAME);
                //b = Bitmap.createScaledBitmap(b, 120, 120, false);
                Bitmap abc = Bitmap.createScaledBitmap(b,400,400, false);
                Drawable dr = new BitmapDrawable(abc);
                imageviewcap.setVisibility(View.VISIBLE);
                imageviewcap.setBackground(dr);

                imageviewcap.animate()
                        .x(1000)
                        .y(0)
                        .setDuration(1000)
                        .start();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                      imageviewcap.setVisibility(View.GONE);
                        imageviewcap.animate()
                                .x(0)
                                .y(0)
                                .setDuration(0)
                                .start();
                    }
                },2000);

                Bitmap bb;
                if (b.getWidth() >= b.getHeight()){

                    bb = Bitmap.createBitmap(
                            b,
                            b.getWidth()/2 - b.getHeight()/2,
                            0,
                            b.getHeight(),
                            b.getHeight()
                    );

                }else{

                    bb = Bitmap.createBitmap(
                            b,
                            0,
                           b.getHeight()/2 - b.getWidth()/2,
                            b.getWidth(),
                            b.getWidth()
                    );
                }

                thumb.setImageBitmap(bb);
                thumb.setVisibility(View.VISIBLE);

                thumb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                        intent.setDataAndType(Uri.parse("file://"+myapp.getStringFromSharedPreferances("VIDEOFILE")), "image/*");
                        startActivity(intent);
                    }
                });

            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    };


    private void focusOnTouch(MotionEvent event) {
        if (myapp.mCamera != null ) {

            try {
                Camera.Parameters parameters = myapp.mCamera.getParameters();
                if (parameters.getMaxNumMeteringAreas() > 0){
                    //Log.i(TAG, "fancy !");
                    Rect rect = calculateFocusArea(event.getX(), event.getY());

                    float xx = event.getX();
                    float yy = event.getY();


                    Log.e("FRAME H & W ", "H : " + myapp.frame.getHeight() + " W : " + myapp.frame.getWidth());

                    myapp.frame.setX(xx - (myapp.frame.getWidth() / 2));
                    myapp.frame.setY(yy - (myapp.frame.getHeight() / 2));

                    myapp.frame.setVisibility(View.VISIBLE);
                    Log.e("Frame Visibilty", ">> " + myapp.frame.getVisibility());

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            myapp.frame.setVisibility(View.INVISIBLE);
                        }
                    }, 4000);

                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
                    List<Camera.Area> meteringAreas = new ArrayList<Camera.Area>();
                    meteringAreas.add(new Camera.Area(rect, 800));
                    parameters.setFocusAreas(meteringAreas);

                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);

                    myapp.mCamera.setParameters(parameters);
                    myapp.mCamera.autoFocus(mAutoFocusTakePictureCallback);
                }else {
                    myapp.mCamera.autoFocus(mAutoFocusTakePictureCallback);
                }
            } catch (Exception e) {
                e.printStackTrace();
                myapp.mCamera.autoFocus(mAutoFocusTakePictureCallback);
            }catch (Throwable t){}
        }
    }

    private Rect calculateFocusArea(float x, float y) {

        int left = clamp(Float.valueOf((x / mPreview.getWidth()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);
        int top = clamp(Float.valueOf((y / mPreview.getHeight()) * 2000 - 1000).intValue(), FOCUS_AREA_SIZE);

        return new Rect(left, top, left + FOCUS_AREA_SIZE, top + FOCUS_AREA_SIZE);
    }

    private int clamp(int touchCoordinateInCameraReper, int focusAreaSize) {
        int result;
        if (Math.abs(touchCoordinateInCameraReper)+focusAreaSize/2>1000){
            if (touchCoordinateInCameraReper>0){
                result = 1000 - focusAreaSize/2;
            } else {
                result = -1000 + focusAreaSize/2;
            }
        } else{
            result = touchCoordinateInCameraReper - focusAreaSize/2;
        }
        return result;
    }
    private Camera.AutoFocusCallback mAutoFocusTakePictureCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            if (success) {
                // do something...
                Log.e("tap_to_focus","success!");

                Camera.Parameters p = myapp.mCamera.getParameters();
                if(myapp.getStringFromSharedPreferances("FLASHMODE")!=null)
                {
                    if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("ON"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    }
                    else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("OFF"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    }
                    else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("AUTO"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                    }
                }
            } else {
                // do something...
                Camera.Parameters p = myapp.mCamera.getParameters();
                if(myapp.getStringFromSharedPreferances("FLASHMODE")!=null)
                {
                    if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("ON"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                    }
                    else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("OFF"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    }
                    else if(myapp.getStringFromSharedPreferances("FLASHMODE").equals("AUTO"))
                    {
                        p.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                    }
                }
                Log.e("tap_to_focus","fail!");
            }
        }
    };
    @Override
    protected void onResume() {
        super.onResume();
        //createCameraSource();
        //startCamSource();

        if(myapp.getStringFromSharedPreferances("CAMERAFLAG")!=null)
        {
            if(myapp.getStringFromSharedPreferances("CAMERAFLAG").equals("1"))
            {
                restartCam(270);
            }
            else
            {
                restartCam(90);
            }
        }
        else {
            restartCam(90);
        }
    }
    /**
     * Stops the camera.
     */
    @Override
    protected void onPause() {
        super.onPause();
        //mPreview.stop();
    }
    /**
     * Releases the resources associated with the camera source, the associated detector, and the
     * rest of the processing pipeline.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mCameraSource != null) {
            mCameraSource.release();
        }
        timer.cancel();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }
    public static Bitmap retriveVideoFrameFromVideo(String videoPath)
            throws Throwable
    {
        Bitmap bitmap = null;
        MediaMetadataRetriever mediaMetadataRetriever = null;
        try
        {
            mediaMetadataRetriever = new MediaMetadataRetriever();
            if (Build.VERSION.SDK_INT >= 14)
                mediaMetadataRetriever.setDataSource(videoPath, new HashMap<String, String>());
            else
                mediaMetadataRetriever.setDataSource(videoPath);
            //   mediaMetadataRetriever.setDataSource(videoPath);
            bitmap = mediaMetadataRetriever.getFrameAtTime();
        }
        catch (Exception e)
        {
            /*e.printStackTrace();*/
            throw new Throwable("Exception in retriveVideoFrameFromVideo(String videoPath)");
        }
        finally
        {
            if (mediaMetadataRetriever != null)
            {
                mediaMetadataRetriever.release();
            }
        }
        return bitmap;
    }

    public void restartCam(final int angle){
        if(mCameraSource!=null)
        mCameraSource.release();
        initFace();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
              //initCam2();
                initCam();
                initRecorder(myapp.isMicrophoneAllow, angle);
            }
        },500);
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {

       /* Log.e("Inside","Preview");
        try {
            Camera.Parameters parameters = camera.getParameters();
            int width = parameters.getPreviewSize().width;
            int height = parameters.getPreviewSize().height;

            YuvImage yuv = new YuvImage(data, parameters.getPreviewFormat(), width, height, null);

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            yuv.compressToJpeg(new Rect(0, 0, width, height), 50, out);

            byte[] bytes = out.toByteArray();
            final Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);

            final FastBitmap image = new FastBitmap(bitmap);
            image.toRGB();

            GaussianBlur gb = new GaussianBlur();
            gb.applyInPlace(image);

            MainView.this.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    demoView.setImageBitmap(bitmap);
                }
            });

        }catch (Exception e){e.printStackTrace();
            Log.e("Error","ERROR");
        }*/
    }



    public class MergeVide extends AsyncTask<String, Integer, String> {

        @Override
        protected void onPreExecute() {
        };
        @Override
        protected String doInBackground(String... params) {
            try {

                Log.e("SIZE",">> "+myapp.fileSegments.size());

                Movie[] inMovies = new Movie[myapp.fileSegments.size()];

                for(int i=0;i<myapp.fileSegments.size();i++)
                {
                    inMovies[i] = MovieCreator.build(myapp.fileSegments.get(i));
                }

                Log.e("SIZE M",">> "+inMovies.length);

                List<Track> videoTracks = new LinkedList<Track>();
                List<Track> audioTracks = new LinkedList<Track>();
                for (Movie m : inMovies) {
                    for (Track t : m.getTracks()) {
                        if (t.getHandler().equals("soun")) {
                            audioTracks.add(t);
                        }
                        if (t.getHandler().equals("vide")) {
                            videoTracks.add(t);
                        }
                    }
                }
                Movie result = new Movie();

                if (audioTracks.size() > 0) {
                    result.addTrack(new AppendTrack(audioTracks
                            .toArray(new Track[audioTracks.size()])));
                }
                if (videoTracks.size() > 0) {
                    result.addTrack(new AppendTrack(videoTracks
                            .toArray(new Track[videoTracks.size()])));
                }

                BasicContainer out = (BasicContainer) new DefaultMp4Builder()
                        .build(result);

                @SuppressWarnings("resource")
                FileChannel fc = new RandomAccessFile(String.format( myapp.defaultPath +"/NEW"+VIDEO_FILENAME  ),"rw").getChannel();
                out.writeContainer(fc);
                fc.close();

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return "Hello";
        }
        @Override
        protected void onPostExecute(String value) {
            super.onPostExecute(value);

            Log.e("Inside", "Completetask");

            File f;
            for(int j=0;j<myapp.fileSegments.size();j++)
            {
                f= new File(myapp.fileSegments.get(j));
                f.delete();
                Log.e("Files","Deleted");
            }

            myapp.fileSegments.clear();
            myapp.addToSharedPreferances("VIDEOFILE", (myapp.defaultPath + "/" + "NEW" + VIDEO_FILENAME));
            myapp.addToSharedPreferances("VIDEOFILETYPE","VIDEO");
            thumb.setVisibility(View.VISIBLE);
            //thumb2.setVisibility(View.VISIBLE);

            try {
                thumb.setImageBitmap(retriveVideoFrameFromVideo((myapp.defaultPath + "/NEW" + VIDEO_FILENAME)));
                thumb.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")));
                        intent.setDataAndType(Uri.parse(myapp.getStringFromSharedPreferances("VIDEOFILE")), "video/mp4");
                        startActivity(intent);
                    }
                });
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }
        }
    }
    public void showUI()
    {
        settingView.setVisibility(View.VISIBLE);
        thumb.setVisibility(View.VISIBLE);
        //thumb2.setVisibility(View.VISIBLE);
        switchcam.setVisibility(View.VISIBLE);
        flash.setVisibility(View.VISIBLE);
        panorama.setVisibility(View.VISIBLE);
    }
    public void hideUI()
    {
        settingView.setVisibility(View.INVISIBLE);
        thumb.setVisibility(View.INVISIBLE);
        //thumb2.setVisibility(View.INVISIBLE);
        switchcam.setVisibility(View.INVISIBLE);
        panorama.setVisibility(View.INVISIBLE);

        if(flag==1)
            flash.setVisibility(View.INVISIBLE);
    }

    private void handleZoom(MotionEvent event, Camera.Parameters params) {

        zoomTV.setVisibility(View.VISIBLE);

        int maxZoom = params.getMaxZoom();
        int zoom = params.getZoom();
        float newDist = getFingerSpacing(event);
        if (newDist > mDist) {
            //zoom in
            if (zoom < maxZoom)
                zoom++;
        } else if (newDist < mDist) {
            //zoom out
            if (zoom > 0)
                zoom--;
        }
        mDist = newDist;
        List<Integer> zooms = null;
        if(params.isZoomSupported()) {
            // get all the zoom levels
            zooms = params.getZoomRatios();
        }
        for (int i=0;i<zooms.size();i++){
            Log.e("zoom","zoom Values:"+zooms.get(i));
        }
        int aspectRatio=params.getZoom();
        int result=zooms.get(aspectRatio);
        float var=result;
        float finalresult=var/100;
        Log.e("result:","resultant aspect ratio:"+finalresult);
        zoomTV.setText(" x" + Float.toString(finalresult)+" ");
        params.setZoom(zoom);
        myapp.mCamera.setParameters(params);
    }
    /** Determine the space between the first two fingers */
    private float getFingerSpacing(MotionEvent event) {
        // ...
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        Log.e("Float values:", "x:y" + (x * x + y * y));
        float var=x*x+y*y;
        return var;
    }
    public class simpleOnScaleGestureListener extends
            ScaleGestureDetector.SimpleOnScaleGestureListener {

        Camera.Parameters cameraParameters;
        int MAX_ZOOM;

        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            Camera.Parameters params = myapp.mCamera.getParameters();
            Log.e("Scale",">> "+String.valueOf(detector.getScaleFactor()));
            return true;
        }
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            Log.e("SCALE","START");
            return true;
        }
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            // TODO Auto-generated method stub
            Log.e("Scale End","END");
            //b=false;
            zoomTV.setVisibility(View.INVISIBLE);
        }
    }

    private class Location extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {


            if (gpsTracker.getIsGPSTrackingEnabled())
            {
                String stringLatitude = String.valueOf(gpsTracker.latitude);
                Log.e("LATITUDE", "??" + gpsTracker.latitude);

                String stringLongitude = String.valueOf(gpsTracker.longitude);
                Log.e("LONGITUDE","??"+gpsTracker.longitude);

                /*String country = gpsTracker.getCountryName(MainView.this);
                Log.e("Country","??"+country);*/

                String city = gpsTracker.getLocality(MainView.this);
                Log.e("City ", "??" + city);

                //String postalCode = gpsTracker.getPostalCode(this);
                //Log.e("LATITUDE", "??" +postalCode);

                //String addressLine = gpsTracker.getAddressLine(this);
                // Log.e("LATITUDE", "??" + addressLine);
            }
            return null;
        }
    }
}