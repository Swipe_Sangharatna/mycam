package com.swipe.mycamera;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class MyPTsansTextView extends TextView {

    public MyPTsansTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public MyPTsansTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyPTsansTextView(Context context) {
        super(context);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "ptssf.ttf");
        setTypeface(tf ,1);
        setTextColor(Color.WHITE);
        setTextSize(getResources().getDimension(R.dimen.fontsettingsize));
    }
}
